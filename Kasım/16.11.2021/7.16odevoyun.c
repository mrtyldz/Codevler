#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "DeckOfCards.h"

#define HANDSIZE 5

/* /////////////////////////////////////////////////// */
/* //////////////// PROTOTYPES /////////////////////// */
/* /////////////////////////////////////////////////// */

int getLine( char [], int );
int getRandomNumber( int );
int playAGame( const int [][13]);
void dealerDiscards( const int [][13], int *, int * );
void playerDiscards( const int [][13], int *, int * );

int compareHands( const int [][13], const int *, const int *, 
                  const int *, const int * );
void dealAHand( const int [][13], int * );
void rankHand( const int [][13], const int *, int * );
void reinitialiseDeck( int [][13] );
void shuffle( int [][13]);
int dealACard( void );

void printBackOfHand( void );
void printHand( const int wDeck[][13], const int *hand);
void printHandRank(int * );
void sortHand( const int [][13], int * );
int getCardFace( const int [][13], int );
int getCardSuit( const int [][13], int );

int isFourOFAKind( const int [][13], const int *, int * );
int isFullHouse( const int [][13], const int *, int * );
int isFlush( const int [][13], const int * );
int isStraight( const int [][13], const int * );
int isThreeOFAKind( const int [][13], const int *, int * );
int isTwoPair( const int [][13], const int *, int * );
int isOnePair( const int [][13], const int *, int * );
void getHighCard( const int [][13], const int *, int * );

int isFourFlush( const int [][13], const int * );
int isOutsideStraight( const int [][13], const int * );

int deckPosition = 1;
const char *suit[4]  = { "Hearts", "Diamonds", "Clubs", "Spades"};
const char *face[13] = { "Ace", "Deuce", "Three", "Four", "Five",
                         "Six", "Seven", "Eight", "Nine", "Ten",
                         "Jack", "Queen", "King" };

enum CARD { ACE, DEUCE, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT,
            NINE, TEN, JACK, QUEEN, KING
};

enum HANDRANKINGS {
    ROYALFLUSH=1, STRAIGHTFLUSH=2, FOUROFAKIND=3, FULLHOUSE=4,
    STRAIGHT=5, FLUSH=6, THREEOFAKIND=7, TWOPAIRS=8, ONEPAIR=9, HIGHCARD=10
};


const char *LOGO = 
"\n\n   '|.   '|'             .|'''.|    .           '||\n"                 
"    |'|   |    ...       ||..  '  .||.   ....    ||  ..    ....   ....\n"  
"    | '|. |  .|  '|.      ''|||.   ||   '' .||   || .'   .|...|| ||. '\n"
"    |   |||  ||   ||   .      '||  ||   .|' ||   ||'|.   ||      . '|..\n"
"   .|.   '|   '|..|'    |'....|'   '|.' '|..'|' .||. ||.  '|...' |'..|'\n\n"
"'||''|.          '||                        \n"
" ||   ||   ...    ||  ..    ....  ... ..   \n"
" ||...|' .|  '|.  || .'   .|...||  ||' ''   \n" 
" ||      ||   ||  ||'|.   ||       ||       \n"
".||.      '|..|' .||. ||.  '|...' .||.     \n\n\n\n\t\t\t";

int main()
{
    int dealerWins=0, playerWins=0, draws=0;
    int winner;
    srand( time(0));
    int deck[4][13] = { {0} };  
    
    char playAgain = 'y';   
    int maxInput = 10;
    char input[maxInput];

    printf("%s", LOGO);
    system("pause");

    while(playAgain=='y' || playAgain=='Y') 
    {
        system("cls");
        reinitialiseDeck( deck );
        shuffle( deck );  

        winner = playAGame( deck );
        
        if(winner==1)
            dealerWins++;
        else if(winner==2)
            playerWins++;
        else
            draws++;

        system("pause");
        system("cls");
        printf("---- STATS ----\n\n");
        printf("Dealer wins = %d\n", dealerWins);
        printf("Player wins = %d\n", playerWins);
        printf("Draws       = %d\n\n", draws);
        
        printf("Would you like to play again (yes/no)?: ");
        getLine(input, maxInput);
        
        if(input[0]=='\n')
            playAgain = 'y';
        else
            playAgain = input[0];
    }

    return 0;
}


int getLine( char s[], int maxLength )
{
    int c, i=0;
    
    while( ( --maxLength > 0)  && ( (c=getchar())!=EOF ) && ( c!='\n' ) )
        s[i++]=c;

    if(c=='\n')
        s[i++]=c;
    s[i] = '\0';

    fflush(stdin);
    
    return i;   
}


int getRandomNumber( int max )
{
    return rand()%max;
}

int playAGame( const int deck[][13])
{   
    int playerHand[HANDSIZE], dealerHand[HANDSIZE];
    int playerHandRank[3] = {0};
    int dealerHandRank[3] = {0};
    int winner;
    
    dealAHand( deck, dealerHand );  
    rankHand( deck, dealerHand , dealerHandRank );     
    
    dealAHand( deck, playerHand  );
    rankHand( deck, playerHand , playerHandRank );     

    printf("---- DEALER HAND ----\n");
    printBackOfHand(); // printHandRank( dealerHandRank );
    dealerDiscards( deck, dealerHand , dealerHandRank );    
    rankHand( deck, dealerHand , dealerHandRank );

    printf("\n---- PLAYER HAND ----\n");
    printHand(deck, playerHand);
    printHandRank( playerHandRank ); 
     
    playerDiscards( deck, playerHand, playerHandRank ); 

    system("cls");     

    printf("---- DEALER HAND ----\n");
    printHand(deck, dealerHand);
    printHandRank( dealerHandRank ); 
    
    printf("\n---- PLAYER HAND ----\n");
    printHand(deck, playerHand);
    printHandRank( playerHandRank ); 
    
    winner = compareHands( deck, dealerHand, playerHand, 
                  dealerHandRank, playerHandRank ); 
    
    if(winner==1)
        printf("Dealer wins!\n");
    else if(winner==2)
        printf("Player wins!\n");
    else
        printf("NOBODY WINS!\n");
        
    return winner;
}


void dealerDiscards( const int wDeck[][13], int *dealerHand, int *dealerHandRank )
{
    int i, card, toDiscard=0;
    //int bluff1 = rand()%10000;
    //int bluff2 = rand()%1000;
    int bluff3 = rand()%100;
    
    int dealerToDiscard[HANDSIZE] = {0};
    
    if(dealerHandRank[0] <= FLUSH) {
        ;
    }
    else if(dealerHandRank[0] == THREEOFAKIND ) 
    {
        for(i=0; i<HANDSIZE; i++) {
            if( getCardFace(wDeck, dealerHand[i]) != dealerHandRank[1]) {
                dealerToDiscard[i]=1;
                toDiscard++;
            }    
        }
    }
    else if(dealerHandRank[0] == TWOPAIRS ) 
    {
        int highPair = dealerHandRank[1];
        int lowPair = dealerHandRank[2];
        
        for(i=0; i<HANDSIZE; i++) 
        {
            card = getCardFace(wDeck, dealerHand[i]);
            if( card != highPair )
            {
                if(card==lowPair && bluff3<60) {
                    if( lowPair < EIGHT ) {
                        dealerToDiscard[i]=1;
                        toDiscard++;
                    }   
                }
                else if(card!=lowPair) {
                    dealerToDiscard[i]=1;
                    toDiscard++;
                }     
            }           
        }
    }
    else if(dealerHandRank[0] == ONEPAIR ) 
    {
        int suit = isFourFlush(wDeck, dealerHand);
        int pair = dealerHandRank[1];
        
        if( (pair<TEN && pair!=ACE) && suit!=-1) {
            for(i=0; i<HANDSIZE; i++) 
            {
                card = getCardSuit(wDeck, dealerHand[i]);
                if( card != suit ) {
                    dealerToDiscard[i]=1;
                    toDiscard++;
                }       
            }
        }
        else {
            for(i=0; i<HANDSIZE; i++) {
                if( getCardFace(wDeck, dealerHand[i]) != dealerHandRank[1]) {
                    dealerToDiscard[i]=1;
                    toDiscard++;
                }       
            }           
        }
    }
    else 
    {
        int suit = isFourFlush(wDeck, dealerHand);
        int outsideStraight = isOutsideStraight(wDeck, dealerHand); 
        
        if(suit!=-1) {
            for(i=0; i<HANDSIZE; i++) {
                card = getCardSuit(wDeck, dealerHand[i]);
                if( card != suit ) {
                    dealerToDiscard[i]=1;
                    toDiscard++;
                }       
            }
        }
        else if(outsideStraight!=-1) {
            dealerToDiscard[outsideStraight]=1;
            toDiscard++;
        }
        else {
            for(i=0; i<HANDSIZE; i++) {
                card = getCardFace(wDeck, dealerHand[i]);
                if( card<QUEEN && card!=ACE ) {
                    dealerToDiscard[i]=1;
                    toDiscard++;
                }       
            }           
        }
    }
    
    if(toDiscard==1)
        printf("Dealer draws 1 card!\n");
    else
        printf("Dealer draws %d cards!\n", toDiscard);
        
    for(i=0; i<HANDSIZE; i++)
        if(dealerToDiscard[i]==1)
            dealerHand[i] = dealACard();
        
    sortHand(wDeck, dealerHand); 
    rankHand(wDeck, dealerHand, dealerHandRank);
}

void playerDiscards( const int wDeck[][13], int *playerHand, int *playerHandRank )
{
    int playerToDiscard[HANDSIZE] = {0};
    int arrLen = HANDSIZE*2+1; 
    char input[arrLen]; 
    
    printf("Choose cards to throw (eg 145): ");
    
    arrLen = getLine(input, arrLen);
    
    char c;
    int cardPos, i=0;
    while( ((c=input[i++])!='\0') && (c!='\n') && (i<arrLen) )
    {
        cardPos = c-49;
        if(cardPos>=0 && cardPos<HANDSIZE)
            playerToDiscard[cardPos] = 1;
    }
    
    for(i=0; i<HANDSIZE; i++) {
        if(playerToDiscard[i]==1)
            playerHand[i] = dealACard();        
    }

    sortHand(wDeck, playerHand); 
    rankHand(wDeck, playerHand, playerHandRank);
}


int compareHands( const int wDeck[][13], 
                   const int *dealerHand, const int *playerHand,
                   const int *dealerHandRank, const int *playerHandRank)
{
    int i, dealerCard, playerCard;
    
    int winner = -1;
    
    int dRank0 = dealerHandRank[0];
    int pRank0 = playerHandRank[0];
    int dRank1 = dealerHandRank[1];
    int pRank1 = playerHandRank[1];
    int dRank2 = dealerHandRank[2];
    int pRank2 = playerHandRank[2];
    
    if( dRank0 < pRank0 )
        winner = 1;             
    else if( pRank0 < dRank0 )
        winner = 2;            
    else if( dRank1==ACE && pRank1!=ACE )
        winner = 1;
    else if(pRank1==ACE && dRank1!=ACE )
        winner = 2; 
    else if( dRank1 > pRank1 )
        winner = 1;
    else if( pRank1 > dRank1 )
        winner = 2; 
    else if(dRank0==TWOPAIRS && dRank2!=pRank2 ) 
    {
        if(dRank2 > pRank2)
            winner = 1;
        else
            winner = 2; 
    }
    else if( ( getCardFace(wDeck, dealerHand[0])==ACE ) && 
             ( getCardFace(wDeck, playerHand[0])!=ACE) )
        winner = 1;
    else if( ( getCardFace(wDeck, playerHand[0])==ACE ) &&
             ( getCardFace(wDeck, dealerHand[0])!=ACE ) )
        winner = 2;  
    else {
        for(i=HANDSIZE-1; i>=0; i--) 
        {
            dealerCard = getCardFace(wDeck, dealerHand[i]);
            playerCard = getCardFace(wDeck, playerHand[i]);
            if( dealerCard > playerCard ) {
                winner = 1;
                break;
            } 
            else if( playerCard > dealerCard ) {    
                winner = 2;
                break;
            }    
        }
    }
    return winner;
}


void dealAHand(const int wDeck[][13], int *hand)
{
    int i;
    int playerHandRank[3] = {0};
    
    for(i=0; i<HANDSIZE; i++)
        hand[i] = dealACard();
        
    sortHand(wDeck, hand); 
    rankHand(wDeck, hand, playerHandRank);
}


void rankHand(const int wDeck[][13], const int *hand, int *rank ) 
{
    int hasStraight, hasFlush;
        
    hasStraight = isStraight(wDeck, hand);
    hasFlush = isFlush(wDeck, hand);
    
    if(hasStraight>=0 && hasFlush>=0) {
        if(hasStraight==TEN)
            rank[0] = ROYALFLUSH;
        else
            rank[0] = STRAIGHTFLUSH;
        rank[1] = hasStraight;
        return;
    }
    
    if( isFourOFAKind(wDeck, hand, rank) == 1)
        return;
    
    if(isFullHouse(wDeck, hand, rank) == 1)
        return;
    
    if(hasStraight >= 0) {
        rank[0] = STRAIGHT;
        rank[1] = hasStraight;
        return;
    }
    
    if(hasFlush >= 0) {
        rank[0] = FLUSH;
        rank[1] = hasFlush;
        return;
    }
    
    if( isThreeOFAKind(wDeck, hand, rank) == 1)
        return;
    
    if( isTwoPair(wDeck, hand, rank) == 1)
        return;
    
    if(isOnePair(wDeck, hand, rank) == 1)
        return;
    
    getHighCard(wDeck, hand, rank);
    return;
}


void reinitialiseDeck( int wDeck[][13] )
{
    deckPosition = 1;
    int startValue = 1;
    
    int row, column;
    
    for( row=0; row<4; row++) {
        for(column=0; column<13; column++)
            wDeck[row][column] = startValue++;  
    }
}


void shuffle( int wDeck[][13] )
{
    deckPosition = 1;
    
    int row, column;
    int randRow, randColumn, temp;
    int pass;
    
    for(pass=0; pass<4; pass++)
    {
        for( row=0; row<1; row++) {
            for(column=0; column<13; column++) {
                randRow    = getRandomNumber(4);
                randColumn = getRandomNumber(13);  
                temp = wDeck[row][column];
                wDeck[row][column] = wDeck[randRow][randColumn];
                wDeck[randRow][randColumn] = temp;
            }   
        }       
    }
}


int dealACard( void )
{
    int card = deckPosition;
    deckPosition++;
    return card;
}


void printBackOfHand() 
{
    int lines=9, characters=12;
    int cards, i, j;
    
    for(i=0; i<lines; i++) 
    {
        for(cards=0; cards<HANDSIZE; cards++) {
            for(j=0; j<characters; j++)
                printf("%c",DECKPICS[ 4 ][ 0 ][ i ][ j ]);
        }
            
        printf("\n");
    }   
}


void printHand( const int wDeck[][13], const int *hand)
{
    int i;

    int suits[HANDSIZE], faces[HANDSIZE];
    for(i=0; i<HANDSIZE; i++) {
        suits[i] = getCardSuit(wDeck, hand[i]);
        faces[i] = getCardFace(wDeck, hand[i]);  
    }

    int lines=9, characters=12;
    int cards, j;
    
    for(i=0; i<lines; i++) 
    {
        for(cards=0; cards<HANDSIZE; cards++) {
            for(j=0; j<characters; j++)
                printf("%c",DECKPICS[ suits[cards] ][ faces[cards] ][ i ][ j ]);
        }
            
        printf("\n");
    }     
}

void printHandRank( int *handRank) 
{
    switch(handRank[0]) 
    {
        case ROYALFLUSH:
            printf("Player has a Royal Flush\n" );
            break;
        case STRAIGHTFLUSH:
            printf("Player has a Straight Flush\n" );
            break;
        case FOUROFAKIND:
            printf("Player has Four %ss\n", face[ handRank[1] ] );
            break;
        case FULLHOUSE:
            printf("Player has a Full House - %ss and %ss\n", 
                    face[ handRank[1]] , face[ handRank[2]] );
            break;
        case STRAIGHT:
            printf("Player has a Straight\n" );
            break;
        case FLUSH:
            printf("Player has a %s high Flush\n", face[ handRank[1]] );
            break;
        case THREEOFAKIND:
            printf("Player has Three %ss\n", face[ handRank[1]] );
            break;
        case TWOPAIRS:
            printf("Player has Two Pairs - %ss and %ss\n", 
                    face[ handRank[1]] , face[ handRank[2]] );
            break;
        case ONEPAIR:
            printf("Player has a Pair of %ss\n", face[ handRank[1]] );
            break;
        default:
            printf("Player has %s high\n", face[ handRank[1]] );
            break;
    }   
}


void sortHand( const int wDeck[][13], int *hand)
{
    int pass, card, temp;

    for(pass=0; pass<HANDSIZE; pass++) {
        for(card=0; card<HANDSIZE-1; card++) {
            if( getCardFace( wDeck, hand[card]) > getCardFace( wDeck, hand[card + 1]) )
            {
                temp = hand[card];
                hand[card]=hand[card+1];
                hand[card+1]=temp;
            }           
        }
    }
}


int getCardFace( const int wDeck[][13], int cardPosition)
{
    int suit, face;
    
    for(suit=0; suit<=3; suit++) {
        for(face=0; face<=12; face++) {
            if(wDeck[suit][face]==cardPosition)
                return face;
        }
    }
    
    return -1;
}


int getCardSuit( const int wDeck[][13], int cardPosition)
{
    int suit, face;
    
    for(suit=0; suit<=3; suit++) {
        for(face=0; face<=12; face++) {
            if(wDeck[suit][face]==cardPosition)
                return suit;
        }
    }
    
    return -1;
}

int isFourOFAKind( const int wDeck[][13], const int *hand, int *rank)
{
    int i, card, count=1;
    int prevCard = getCardFace(wDeck, hand[0]);
    
    for(i=1; i<HANDSIZE; i++) 
    {
        card = getCardFace(wDeck, hand[i]);
        
        if(card==prevCard) {
            if(++count==4) {
                rank[0] = FOUROFAKIND;
                rank[1] = card;
                return 1;
            }
        }
        else
            count=1;
        
        prevCard = card;
    }

    return -1;
}

int isThreeOFAKind( const int wDeck[][13], const int *hand, int *rank)
{
    int i, card, count=1;
    int prevCard = getCardFace(wDeck, hand[0]);
    
    for(i=1; i<HANDSIZE; i++) 
    {
        card = getCardFace(wDeck, hand[i]);
        
        if(card==prevCard) {
            if(++count==3) {
                rank[0] = THREEOFAKIND;
                rank[1] = card;
                return 1;
            }
        }
        else
            count=1;
        
        prevCard = card;
    }

    return -1;
}

int isFlush( const int wDeck[][13], const int *hand)
{
    int i, card;
    int suit = getCardSuit(wDeck, hand[0]);
    int isFlush=getCardFace(wDeck, hand[0]);
    
    for(i=1; i<HANDSIZE; i++) 
    {
        card = getCardSuit(wDeck, hand[i]);
        
        if(card!=suit) 
        {
            return -1;
        }
        else if(isFlush!=ACE) {
            isFlush = getCardFace(wDeck, hand[i]);
        } 
    }

    return isFlush;
}

int isStraight( const int wDeck[][13], const int *hand)
{
    int i, card;
    int prevFace = getCardFace(wDeck, hand[0]);
    int hasStraight = prevFace;
    
    for(i=1; i<HANDSIZE; i++) 
    {
        card = getCardFace(wDeck, hand[i]);
        if(hasStraight==ACE && i==1) 
        {
            if(card!=DEUCE && card!=TEN) {
                return -1;      
            }
            else if(card==TEN)
                hasStraight=TEN;
        }
        else if(card != prevFace+1)
        {
            return -1;
        }
        
        prevFace = card;
    }

    return hasStraight;
}

int isFullHouse( const int wDeck[][13], const int *hand, int *rank)
{
    int i, card, count=1;
    int prevCard = getCardFace(wDeck, hand[0]);
    int hasThreeOfKind = -1;
    int hasPair = -1;

    
    for(i=1; i<HANDSIZE; i++) 
    {
        card = getCardFace(wDeck, hand[i]);
        
        if(card==prevCard) 
        {
            count++;
            if(count==2 && hasPair==-1) {
                hasPair = card;
            }
            if(count==3) 
            {
                hasThreeOfKind = card;
                if(i==2) 
                    hasPair = -1;
            }   
        }
        else if( i==1 ) 
            return -1; 
        else
            count=1;
        
        prevCard = card;
    }
    
    if(hasPair>=0 && hasThreeOfKind>=0) {
        rank[0] = FULLHOUSE;
        rank[1] = hasThreeOfKind;
        rank[2] = hasPair;
        return 1;
    } 
    else
        return -1;
}


int isTwoPair( const int wDeck[][13], const int *hand, int *rank)
{
    int i, card;
    int prevCard = getCardFace(wDeck, hand[0]);
    int pairs = 0;
    int highPair = -1;
    int lowPair = -1;
    
    for(i=1; i<HANDSIZE; i++) 
    {
        card = getCardFace(wDeck, hand[i]);
        
        if(card==prevCard) {
            pairs++;
            
            if(pairs==1)
                highPair = card;    
            else if( highPair!=ACE && card>highPair) {
                lowPair = highPair;
                highPair = card;
            }
            else
                lowPair = card;
        }
        
        prevCard = card;
    }
    
    if(pairs == 2) {
        rank[0] = TWOPAIRS;
        rank[1] = highPair;
        rank[2] = lowPair;
        return 1;
    }
    else
        return -1;
}


int isOnePair( const int wDeck[][13], const int *hand, int *rank)
{
    int i, card;
    int prevCard = getCardFace(wDeck, hand[0]);
    
    for(i=1; i<HANDSIZE; i++) 
    {
        card = getCardFace(wDeck, hand[i]);
        
        if(card==prevCard) {
            rank[0] = ONEPAIR;
            rank[1] = card;
            return 1;           
        }   
        
        prevCard = card;
    }
    
    return -1;
}


void getHighCard( const int wDeck[][13], const int *hand, int *rank)
{
    rank[0] = HIGHCARD;

    if(getCardFace(wDeck, hand[0]) == ACE )
        rank[1] = ACE;
    else
        rank[1] = getCardFace(wDeck, hand[HANDSIZE-1]); 
}

int isFourFlush( const int wDeck[][13], const int *hand)
{
    int i;
    int suit[4] = {0}; /* { "Hearts", "Diamonds", "Clubs", "Spades"} */
    

    for(i=0; i<HANDSIZE; i++) 
    {
        ++suit[getCardSuit(wDeck, hand[i])];
    }

    for(i=0; i<4; i++) {
        if(suit[i]==4)
            return i;
    }
    return -1;
}


int isOutsideStraight( const int wDeck[][13], const int *hand)
{
    int i, card, count=1;
    int prevFace = getCardFace(wDeck, hand[0]);
    
    for(i=1; i<HANDSIZE; i++) 
    {
        card = getCardFace(wDeck, hand[i]);
        
        if( (i==1 || i==2) && prevFace==ACE ) 
        {
            if(card!=DEUCE && card!=JACK)
                count=1;    
            else
                count++;;
        }
        else if(card != prevFace+1)
        {
            if(i==1)
                count=1;
            else if(i!=1 && i!=HANDSIZE-1)
                return -1;
            else if(i==HANDSIZE-1 && count==HANDSIZE-1) {
                return HANDSIZE-1;
            }
        }
        else
            count++;
        
        prevFace = card;
    }
    
    if(count==HANDSIZE-1)
        return 0;
    else
        return -1;
}

