#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <locale.h>
#define GAMES 5000

int rollDice( void );
int getRandomNum( int, int );


int main()
{
	setlocale(LC_ALL, "turkish");
    int gameStatus, sum, myPoint;
    
    int winRolls[22]  = {0};
    int loseRolls[22] = {0};
    int totalWon=0, totalLost=0;
    
    srand(time( NULL )); 
        
    int rollNumber;
    int totalRolls = 0;
	
    int i;
	
    for(i=1; i<=GAMES; i++)
    {	
	gameStatus = 0;
        sum = rollDice();
	rollNumber = 1;
		
	switch(sum) 
	{
            case 7: case 11:
		gameStatus = 1;
		break;
   	    case 2: case 3: case 12:
		gameStatus = 2;
		break;
 	    default:
		gameStatus = 0;
		myPoint = sum;
		break;
	}
		
	while(gameStatus == 0)
	{
	    ++rollNumber;	
	    sum = rollDice();
			
	    if(sum == myPoint)
	        gameStatus = 1;
	    else if(sum == 7)
	        gameStatus = 2; 
	}
	    
	totalRolls += rollNumber;
		
	if(gameStatus == 1) {
	    if(rollNumber <= 20)
	        ++winRolls[rollNumber];
	    else
	        ++winRolls[21];
	}	    
        else {
	    if(rollNumber <= 20)
	        ++loseRolls[rollNumber];
	    else
	        ++loseRolls[21];
	}
    }
    
    printf("KAZANAN SAYILAR\n");
	
    for(i=1; i<21; i++) {
    	if(i<10)
    	    printf("%4d", i);
    	else
    	    printf("%3d", i);
	}
    printf(" >20\n");

    for(i=1; i<=21; i++) {
    	totalWon += winRolls[i];
    	
    	if(i<10)
        	printf("%4d", winRolls[i]);
    	else
        	printf("%3d", winRolls[i]);
	}

    printf("\n\nKAYBEDEN SAYILAR\n");
    for(i=1; i<21; i++) {
    	if(i<10)
    	    printf("%4d", i);
    	else
    	    printf("%3d", i);
	}
    printf(" >20\n");

    for(i=1; i<=21; i++) {
    	totalLost += loseRolls[i];
    	
    	if(i<10)
            printf("%4d", loseRolls[i]);
    	else
            printf("%3d", loseRolls[i]);
    }

    printf("\n\nKazanılan oyunlar:\t%3d\n", totalWon);
    printf("Kaybedilen oyunlar:\t%3d\n", totalLost);
    printf("Ortalama oyun uzunluğu:\t%3d\n", totalRolls/GAMES);

    return 0;
}


int rollDice( void )
{
    int die1, die2, worksum;
	
    die1 = getRandomNum(1, 6);
    die2 = getRandomNum(1, 6);
    worksum = die1 + die2;
	
    return worksum;
}


int getRandomNum(int min, int max)
{
    int res = min + rand()%max;
    return res;
}