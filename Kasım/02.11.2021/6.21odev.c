#include <stdio.h>
#include<locale.h>

#define MAXSEATS 10

const char* MENU = 
"\n Birinci sinif icin 1'i tuslayiniz"
"\n Ikinci sinif icin 2'yi tuslayiniz";

    
const char* MENU1 = 
"\nBirinci sinif doludur."
"\nİkinci sinif2 ister misiniz?"
"\nIsterseniz 1'i, istemezseniz 2'yi tuslayin.";


const char* MENU2 = 
"\nİkinci sinif doludur."
"\nBirinci sinif ister misiniz?"
"\nIsterseniz 1'i, istemezseniz 2'yi tuslayin.";


void printPass( int );
int assignFirstClassSeat( int [] );
int assignEconomySeat( int [] );
int acceptAlternative( const char* );


void printPass( int seatNum )
{
    printf("\n---------------------------------------\n");
    if(seatNum < 5)
    printf("              BIRINCI SINIF\n");
    else
    printf("              IKINCI SINIF\n");
    printf("---------------------------------------\n");
    printf("Ucus TR031\tKalkis\tKoltuk No\n");
    printf("To Istanbul\t  9:10\t%4d\n", seatNum+1);
    printf("\nKalkis 9:40 Kapi 15\n");
    printf("---------------------------------------\n\n");
}


int assignFirstClassSeat(int seats[])
{
    int seatAssigned = 0;
    int i;
	
    for(i=0; i<5; i++)
    {
	if(seats[i]==0)
	{
            seats[i]=1;
            seatAssigned = 1;
	    printPass( i );
	    break;
	} 
    }
	
    return seatAssigned;	
}


int assignEconomySeat(int seats[])
{
    int seatAssigned = 0;
    int i;
	
    for(i=5; i<10; i++)
    {
	if(seats[i]==0)
	{
	    seats[i]=1;
	    seatAssigned = 1;
	    printPass( i );
	    break;
	} 
    }
	
    return seatAssigned;	
}


int acceptAlternative(const char *menu)
{
    int choice;
    printf(menu);
    scanf("%d", &choice);
	
    return choice==1;
}


int main()
{
    setlocale(LC_ALL, "turkish");
    int seatsTaken = 0;
    int choice;
    int seatsAvailable[ MAXSEATS ] = { 0 };
	
    while(seatsTaken < MAXSEATS )
    {
    	printf(MENU);
    	scanf("%d", &choice);
    	
    	if(choice==1)
    	{
            if(assignFirstClassSeat(seatsAvailable) == 0 )
    	    {
    		if(acceptAlternative(MENU1) == 1) {
    		    assignEconomySeat(seatsAvailable); 
    		    seatsTaken++;
		}
		else 
		    printf("\nDiger ucus 3 saat sonra!\n");
	    }
	    else {
		seatsTaken++;
	    }
	}
    	else if(choice==2)
    	{
    	    if(assignEconomySeat(seatsAvailable) == 0 )
    	    {
    		if(acceptAlternative(MENU2) == 1) {
    		    assignFirstClassSeat(seatsAvailable); 
    		    seatsTaken++;
		}
		else
		    printf("\nDiger ucus 3 saat sonra!\n");
		}
	    else {
	        seatsTaken++;
	    }
	}
	else {
	    printf("\nHatali menu girisi!\n");	
	}
    }	
    printf("\nTum biletler satildi!\n");
	
    return 0;
}