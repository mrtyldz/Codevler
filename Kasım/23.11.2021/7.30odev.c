#include<stdio.h>  
#include<locale.h>

void area_peri(float, float*, float*);

int main()
{
    setlocale(LC_ALL, "turkish");
    float radius, area, perimeter;

    printf("DAİRENİN YARIÇAPINI GİRİNİZ:");
    scanf_s("%f", &radius);

    area_peri(radius, &area, &perimeter);

    printf("\nDAİRENİN ALANI = %0.2f\n", area);
    printf("DARENİN ÇEVRESİ = %0.2f\n", perimeter);

    return 0;
}

void area_peri(float r, float* a, float* p)
{
    *a = 3.14 * r * r;
    *p = 2 * 3.14 * r;
}