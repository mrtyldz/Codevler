#include<stdio.h>
#include<locale.h>
#define SIZE 15

int binarySearch(int b[], int low, int mid, int high);
void pirntHeader(void);
void printRow(int b[], int low, int mid, int high);

int main() {
	setlocale(LC_ALL, "turkish");
	int a[SIZE];
	int i;
	int key;
	int result;

	for (i = 0; i < SIZE; i++) {
		a[i] = 2 * i;
	}
	printf("0 İLE 28 ARASINDA BİR SAYI GİRİNİZ:");
	scanf_s("%d", &key);

	pirntHeader();

	result = binarySearch(a, key, 0, SIZE - 1);

	if (result != -1) {
		printf("\n%d DİZİ ELEMANINDA %d SAYISI BULUNDU\n", result, key);
	}
	else {
		printf("\n %d SAYISI BULUNAMADI\n", key);
	}
	return 0;
}

int binarySearch(int b[], int searchKey, int low, int high) {

	int middle;

	if (low <= high) {
		middle = (low + high) / 2;
		printRow(b, low, middle, high);

		if (searchKey == b[middle]) {
			return middle;
		}
		else if (searchKey < b[middle]) {
			return binarySearch(b, searchKey, low, middle - 1);
		}
		else {
			return binarySearch(b, searchKey, middle + 1, high);
		}
	}
	return -1;
}
void pirntHeader(void) {

	int i;
	printf("\nABONELİKLER:\n\n");

	for (i = 0; i < SIZE; i++) {
		printf("%3d ", i);
	}
	printf("\n");
	for (i = 1; i <= 4 * SIZE; i++) {
		printf("-");
	}
	printf("\n");
}

void printRow(int b[], int low, int mid, int high) {
	int i;
	for (i = 0; i < SIZE; i++) {

		if (i < low || i > high) {
			printf("   ");
		}
		else if (i == mid) {
			printf("%3d*", b[i]);
		}
		else {
			printf("%3d", b[i]);
		}
	}
	printf("\n");
}