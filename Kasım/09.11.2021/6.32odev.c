#include<stdio.h>
#include<locale.h>
#define SIZE 100
int linearSearch(int array[], int key, int low, int high);

int main() {
	setlocale(LC_ALL, "turkish");
 
	int array[SIZE];
	int loop;
	int searchkey;
	int element;

	for (loop = 0; loop < SIZE; loop++) {
		array[loop] = 2 * loop;
	}

	printf("TAM SAYI ARAMA ANAHTARINI GİRİNİZ:");
	scanf_s("%d", &searchkey);
	element = linearSearch(array, searchkey, 0, SIZE - 1);

	if (element != -1) {
		printf("\n%d ÖĞESİNDE DEĞER BULUNDU\n",element);
	}
	else {
		printf("\nDEĞER BULUNAMADI");
	}
	return 0;
}

int linearSearch(int array[], int key, int low, int high) {

	if (array[low] == key) {
		return low;
}
	else if (low == high) {
		return -1;
	}
	else {
		return linearSearch(array, key, low + 1, high);
	}
  }