#include <stdio.h>

int main(){

   int sayi; //sayi diye tam sayi tipinde degisken belirle

   printf("Bir sayı giriniz : "); //sayi iste

   scanf("%d",&sayi); //sayiyi gir

   printf("\n5ci basamak %d", sayi%10); //10la modunu al
   printf("\n4cu basamak %d", (sayi/10)%10); //100le modunu al
   printf("\n3cu basamak %d", (sayi/100)%10);
   printf("\n2ci basamak %d", (sayi/1000)%10);
   printf("\n1ci basamak %d", (sayi/10000)%10);

   return 0; // fonksiyon 0 çıktısı versin
}
