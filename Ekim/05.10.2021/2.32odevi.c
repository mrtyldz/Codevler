#include <stdio.h>
#include <math.h>

int main(){

   float cm=0,kg=0,boy=0,vki=0; //degiskenleri belirle
   printf("boyunuzu cm olarak giriniz: "); //boy al
   scanf("%f", &boy);
   printf("kgnizi giriniz: "); //kg al
   scanf("%f", &kg);
   cm=boy/100; // boyu metreye cevir
   vki = kg/pow(cm,2);  //vki hesapla
   printf("Vucut Kitle indeks degeriniz: %0.2f",vki); //vki nin son 3 basamagini vermeden cikti ver
   printf("\nSiniflandirmaniz :   "); //vki bicimlerine gore sınıflandırmasını ver
   
   if (vki <20)
   {
       printf("Zayif");
   }
   else if(vki==20 || vki<25 )
   {
       printf("Normal");
   }
       else if(vki==25 || vki<30 )
   {
       printf("Hafif Sisman");
   }
       else if(vki==30 || vki<35 )
   {
       printf("Sisman");
   }
       else if(vki==35 || vki<45)
   {
       printf("Saglik Acisindan Onemli");
   }
       else if(vki==45 || vki<50 )
   {
       printf("Asiri Sisman");
   }
   else if(vki>=50)
   {
       printf("Morbid(Olumcul) Sisman");
   }

   return 0; //fonksiyon 0 ciktisi versin

}
