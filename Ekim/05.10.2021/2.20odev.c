#include <stdio.h>

int main() {

   float pi = 3.14159; //pi yi belirle
   float yaricap; //yaricap diye degisken olustur

   printf("\nyaricapi giriniz: "); // yaricapi iste

   scanf("%f", &yaricap); //yaricapı gir
   
   printf("\ncemberin capi: %f", yaricap*2);
   printf("\ncemberin cevresi: %f", 2*pi*yaricap);
   printf("\ndairenin alani: %f", pi*yaricap*yaricap);
   printf("\nkurenin hacmi: %f", 4*(pi*yaricap*yaricap*yaricap)/3);
   
   return 0; // fonksiyon 0 çıktısı versin

}

