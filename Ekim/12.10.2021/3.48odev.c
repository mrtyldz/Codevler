/*
    3.48 c odevi sifreleme ve cozme
    Tek Kod
*/

#include <stdio.h>

int main(){

	int sayi=0,birinci=0,ikinci=0,ucuncu=0,dortuncu=0,giris=0; //degisken belirle
	printf("4 basamakli sayi giriniz: "); //sayi iste
	scanf("%d", &sayi); //sayi al

    printf("sifrelemek istiyorsaniz 1 cozdurmek istiyorsaniz 2 giriniz");
    scanf("%d", &giris);

    switch (giris)
    {
    case 1:

    birinci=sayi%10;
	ikinci=(sayi/10)%10;		//basamaklara ayir
	ucuncu=(sayi/100)%10;
	dortuncu=(sayi/1000)%10;
	

	birinci=(birinci+7)%10;
	ikinci=(ikinci+7)%10;
	ucuncu=(ucuncu+7)%10; 		//sifreleme islemini yap
	dortuncu=(dortuncu+7)%10;

	printf("\nGirdiginiz sayi: %d", sayi);
	printf("\nSifrelenmis sayiniz: %d%d%d%d", ucuncu,dortuncu,birinci,ikinci);

        break;
    
    case 2:

    birinci=sayi%10;
	ikinci=(sayi/10)%10;		//basamaklara ayir
	ucuncu=(sayi/100)%10;
	dortuncu=(sayi/1000)%10;
	
	birinci=(birinci+3)%10;
	ikinci=(ikinci+3)%10;
	ucuncu=(ucuncu+3)%10; 		//cozme islemini yap  3 daha eklersek toplamda 10 eklemis oluruz 
	dortuncu=(dortuncu+3)%10;

    printf("\nGirdiginiz sayi: %d", sayi);
	printf("\nSifresi cozulmus sayiniz: %d%d%d%d", ucuncu,dortuncu,birinci,ikinci);

        break;

    default:
        printf("Yanlis giris yaptiniz!");
        break;
    }

}