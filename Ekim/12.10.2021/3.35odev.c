#include <stdio.h>

int main (){

	int numara,binary,decimal=0,taban=1,a; //kullanıcıdan alacagimiz sayi numara, binary 2lik sayi, decimal 10luk sayi, taban binaryden decimale donustururken kullanacagiz, ve a degiskeni
	printf("2lik numara giriniz:\n"); //sayi iste
	scanf("%d", &numara); //sayi al
	binary=numara; //2lik sayiyi aldigin sayiya esitle

	while(numara>0){
		a=numara%10; //10la modunu al
		decimal=decimal+a*taban; // onluk sayi = onluk sayi + 10dan kalan modu * 2nin üstleri (taban başta 2^0 dan 1dir)
		numara = numara/10; //numarayı küçült her seferinde numara 0dan büyük olana kadar döngü devam edicek
		taban = taban * 2; //tabanı her seferde 2^ nin katları olacak şekilde arttır
	}

	printf("2lik numara: %d", binary); //2ligi yazdir
	printf("\n 10luk numara %d", decimal); //10lugu yazdir

    return 0 ;
}

/* algoritma mantığı
    1   0   1   0   1  binary
    2^4 2^3 2^2 2^1 2^0 decimal şekline çevrilişi
    16  0   4   0   1 decimal halde yazılışı
    toplamı 21
*/