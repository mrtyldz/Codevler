#include <stdio.h>

float fiyatgetir(int x){ //while içine de yazabilirdim fakat dışarıdan çağırtmak daha kolay geldi

    float fiyat = 0.0;

    switch (x){
    
    case 1:
        fiyat=2.98;
        break;

    case 2:
        fiyat=4.50;
        break;

    case 3:
        fiyat=9.98;
        break;

    case 4:
        fiyat=4.49;
        break;

    case 5:
        fiyat=6.87;
        break;

    default:
        printf("\nlutfen gecerli bir urun numarasi giriniz!");
        fiyat=-1;
        break;
    }

    return fiyat;
}


int main(){

    int urun=0,satismiktari=0;
    float fiyat=0.0;
    float toplampara=0.0; //degisken belirle

    while (urun!=-1){ //donguye sok
        printf("\nurun numarasini giriniz (cikmak icin -1 yaziniz): ");
        scanf("%d", &urun); //urunun numarasini al


    if(urun!=-1){
        fiyat = fiyatgetir(urun); // fiyatı aldıgın numaraya gore esitle
    }
    else
        fiyat = -1;

        if((int)fiyat!=-1){ //fiyat -1 degilse yani usteki sorgu hatali degilse
            printf("\nkac tane urun satildigini giriniz: ");
            scanf("%d", &satismiktari); //kac tane satildigini al

            if(satismiktari>0){
                toplampara=toplampara+(fiyat*satismiktari); //0dan fazla satilmissa toplam satisi bul
            }

        }

    }   
    
    printf("\ntoplam para: %0.2f", toplampara); //ekrana kac para kazandigini yaz

}