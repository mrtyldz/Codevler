/* binaryden decimale algoritma mantığı 
    1   0   1   0   1  binary
    2^4 2^3 2^2 2^1 2^0 decimal şekline çevrilişi
    16  0   4   0   1 decimal halde yazılışı
    toplamı 21
*/

/* #include <stdio.h>

int main()
{   
    int i;
    printf("Decimal\tBinary\tOctal\tHex\n");

    for(i=0; i<=256; i++) // decimal max 256 oluyor
    {
        printf("%d\t", i);
        ikilikcevirme(i);
        printf("\t%o\t%X\n", i, i); 
    }

    return 0;
}

int ikilikcevirme(int x) 
{
    int kuv=1;
    
    while(kuv<=x)
        kuv*=2;
        
    kuv/=2;
    
    while(kuv>1) 
    {
        if(x>=kuv) {
            printf("%d",(x/kuv));
            x-=kuv;
        }
        else
            printf("0");
        
        kuv/=2;
    }
    
    printf("%d", x);
}
*/

#include <stdio.h>

int ikilikcevirme(int x){

    int a,b;

    for (a=8; a>=0; a--) // a nın sekiz olmasının nedeni 256 sayısından dolayı yine anlamazsan arastir ogren
    {
        b= x >> a; // http://www.lojikprob.com/c/temel-c-programlama-46-bit-manipulasyonu-ve-bitwise-operatorleri/

        if ( b & 1) // https://necatiergin2019.medium.com/bitsel-i%CC%87%C5%9Flemler-bitwise-operations-e1ad30d605cc
        printf("1");

        else  // https://www.youtube.com/watch?v=Taik4BreiWw
        printf("0");
    }
    
    //printf("%d",x);
    //return x;

}

int main(){

    int decimal=0;

    printf("8lik    10luk   16lik   char tipi   2lik\n\t\t\t");

    while (decimal<=256){

        printf("\n",ikilikcevirme(decimal));
        printf("%o\t%d\t%x\t%c\t", decimal,decimal,decimal,decimal);

        decimal++;
    }
    
}