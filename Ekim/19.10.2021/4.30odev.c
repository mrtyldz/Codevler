/*
#include <stdio.h>

int main(void)
{
   unsigned int aCount = 0; 
   unsigned int bCount = 0; 
   unsigned int cCount = 0; 
   unsigned int dCount = 0;
   unsigned int fCount = 0; 

   puts("Enter the letter grades." );
   puts("Enter the EOF character to end input." );
   int grade; // one grade 

   // loop until user types end-of-file key sequence
   while ((grade = getchar()) != EOF) {
      
      // determine which grade was input
      switch (grade) { // switch nested in while

         case 'A': // grade was uppercase A
         case 'a': // or lowercase a
            ++aCount; 
            break; // necessary to exit switch

         case 'B': // grade was uppercase B
         case 'b': // or lowercase b
            ++bCount;
            break;

         case 'C': // grade was uppercase C
         case 'c': // or lowercase c
            ++cCount; 
            break;

         case 'D': // grade was uppercase D
         case 'd': // or lowercase d
            ++dCount; 
            break;

         case 'F': // grade was uppercase F
         case 'f': // or lowercase f
            ++fCount;
            break; 

         case '\n': // ignore newlines,
         case '\t': // tabs,
         case ' ': // and spaces in input
            break; 

         default: // catch all other characters
            printf("%s", "Incorrect letter grade entered."); 
            puts(" Enter a new grade."); 
            break; // optional; will exit switch anyway
      } 
   } // end while

   // output summary of results
   puts("\nTotals for each letter grade are:");
   printf("A: %u\n", aCount);
   printf("B: %u\n", bCount); 
   printf("C: %u\n", cCount); 
   printf("D: %u\n", dCount); 
   printf("F: %u\n", fCount); 
} 
*/

#include <stdio.h>

int main(){
    
    int grade;
    unsigned int aCount=0, bCount=0, cCount=0, dCount=0, fCount=0;

    printf("Enter the letter grades." );
    printf("\nEnter the EOF character to end input." );

    while ((grade = getchar()) != EOF) {
        
        if(grade=='A' || grade=='a'){++aCount;}
        
        else if(grade=='B' || grade=='b'){bCount++;}
        
        else if(grade=='C' || grade=='c'){cCount++;}
        
        else if(grade=='D' || grade=='d'){dCount++;}
        
        else if(grade=='F' || grade=='f'){fCount++;}
        
        else if(grade=='\n' || grade==' ' || grade=='\t'){}

        else {
            printf("%s","Incorrect letter grade entered.\n" );
            printf("Enter a new grade.\n");
        }

    }
    
    printf( "Totals for each letter grade are:\n" );
    printf( "A: %d\n", aCount);    
    printf( "B: %d\n", bCount);
    printf( "C: %d\n", cCount);
    printf( "D: %d\n", dCount);    
    printf( "F: %d\n", fCount);    

}