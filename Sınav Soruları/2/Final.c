/*************************************************************/
/*                                                           */
/*                  Written by                               */
/*                 Erdem Dilmen                              */
/*           Mechatronics Engineering                        */
/*              Pamukkale University                         */
/*                  4.1.2022                                 */
/*                                                           */
/*************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <io.h>

#define _USE_MATH_DEFINES   // pi sayısını kullanmak isterseniz bu gereklidir
                            // bu sayı, M_PI sembolik sabiti ile gösterilmektedir
#include <math.h>           // pi sayısının kullanılabilmesi için dahil edilir

int SML_sim(const char* SML_file); // SML makine dilinde benzetimcisi (simulator) - fonksiyon prototipi

#define CODEMEMSIZE 100 // kod hafızasının içerdiği kelime (word) sayısı cinsinden boyutu

#define INREGCNT 2  // girdi yazmacı sayısı 2 olarak belirlenmiştir
#define OUTREGCNT 1 // // çıktı yazmacı sayısı 1 olarak belirlenmiştir

#define REGSTARTADDRESS CODEMEMSIZE // yazmaçların başlangıç adresi kod hafızasının boyutuna eşittir
                                    // kod hafızasının bittiği konumdan yazmaçlar hafızada yerleşmeye başlar

#define ACCADDRESS REGSTARTADDRESS // akümülatörün adresi - ilk yazmaç akümülatördür

#define INREGSTARTADDRESS (ACCADDRESS + 1) // girdi yazmaçlarının başlangıç adresi - hemen akümülatörden sonra başlar
#define INREGENDADDRESS (ACCADDRESS + INREGCNT) // girdi yazmaçlarının bitiş adresi

#define OUTREGSTARTADDRESS (INREGENDADDRESS + 1) // çıktı yazmaçlarının başlangıç adresi - hemen girdi yazmaçlarından sonra başlar
#define OUTREGENDADDRESS (INREGENDADDRESS + OUTREGCNT) // çıktı yazmaçlarının bitiş adresi


// aşağıdaki yazmaçlar SML dilinde yazılacak fonksiyonların girdi ve çıktılarını tutar
double inreg[INREGCNT]; // girdi yazmaçları (fonksiyon girdisinin değerini tutar - input register) - hafıza konumu 101-102
double outreg[OUTREGCNT]; // çıktı yazmaçları (fonksiyonun döndüreceği değerini tutar - input register) - hafıza konumu 103


#define PATH_MAX 500    // üzerinde çalışılan klasördeki SML dosyasının tam adını tutacak (kalsör adresi şeklinde)
#define MAX_LENGTH 500  // SML dosyasından her bir satır okunurken okunacak en fazla karakter sayısı

#define _DEBUG 0

// fonksiyon tanımı: hafızaya yazılmış olan komutları gösterir
// @in: memory - hafızadaki komutları tutan dizi, size - bu dizinin boyutu
// @out: void
void displayMemory(double memory[], int size) {

    int i;

    for (i = 0; i < size; i++) {
        if (!(memory[i] - (long long int)memory[i])) // eğer hafızada saklanan değer tam sayı türünde ise
            printf(" %010lld ", (long long int)memory[i]);
        else // ondalıklı sayı türünde ise
            printf(" %010.3lf ", memory[i]);

        if ((i + 1) % 10 == 0) {
            printf("\n");
        }
    }

    printf("\n");

}

// ANA FONKSİYON

void main() {

    double angle_d = 89.999;    // derece cinsinden açı
    double sinval;              // bu açının sinusu
    double asinval;             // bu sinus degerinin ters sinusu (açının kendisini vermesi beklenir)
    const char* SML_file_sin_ = "SML_sin_.txt";  // sinus fonksiyonu için SML dili kodlarını içeren dosya - SML_sin_.txt
    const char* SML_file_asin_ = "SML_asin_.txt";  // ters sinus fonksiyonu için SML dili kodlarını içeren dosya - SML_asin_.txt

    double angle_r = angle_d * (2 * M_PI / 360);
    inreg[0] = angle_r; // girdi (SML dosyası içindeki fonksiyon olarak çalışan kodun girdisi - inreg içinde tutulur)
    printf("\n\tSINUS HESAPLANIYOR\t\n");
    SML_sim(SML_file_sin_); // bilgisayar çalışıyor - sinus hesabı
    sinval = outreg[0]; // çıktı (SML dosyası içindeki kodun döndürdüğü değer - outreg içinde tutulur)

    inreg[0] = sinval;
    printf("\n\tTERS SINUS HESAPLANIYOR\t\n");
    SML_sim(SML_file_asin_); // bilgisayar tekrar çalışıyor - ters sinus hesabı
    asinval = (360 / (2 * M_PI)) * outreg[0];

    printf("aci: %.10lf => sin_(aci): %.10lf\n", angle_d, sinval);
    printf("deger=sin_(aci): %.10lf => aci derece cinsinden=asin_(deger): %.10lf\n", sinval, asinval);

    double error = angle_d - asinval; // ters sinus sonucu ile açı arasındaki hata - ondalıklı sayı
    double student_score; // öğrenci puanı
    if (error < 0)
        error *= -1;    // hatanın mutlak değeri gerekli
    printf("\nHATA: %lf\n", error);

    if (error >= 0.1) // eğer hata 0.1'den büyük veya eşit bir değere sahipse öğrenci 0 puan alır
        student_score = 0;
    else if (error >= 0.05 && error < 0.1) // bu hata aralığı için öğrenci paunı şöyle hesaplanır
        student_score = (0.1 - error) * 2000;
    else            // hata 0.05'ten küçük ise öğrenci 100 puan alır
        student_score = 100;

    puts("");
    printf("\n\tOGRENCI PUANI: %lf\n", student_score);

}

// fonksiyon tanımı: SML makine dilinde yazılan komutları dosyada okuyup çalıştırır
// @in: SML_file - SML komutlarının okunacağı dosyanın ismi
// @out: 1 - başarı durumunda, -1 - başarısızlık durumunda
int SML_sim(const char* SML_file) {

    double memory[CODEMEMSIZE] = { 0 }; // hafızaya yazılacak makine dili komutlarını tutar
    long long int i = 0; // çalıştırılacak komutun bulunduğu adres değerini tutan program sayacı
    long long int opcode = 0; // çalıştırılacak komut
    long long int address = 0; // komutun işleyeceği argüman yani adres
    double accumulator = 0; // aritmetik işlemler için özel yazmaç (register) - hafıza konumu 100
    int counter = 0; // SML komutlarını ilgili dosyadan çekip hafızaya kaydederken kullanılan sayaç
    FILE* file; // SML.txt isimli ve SML komutlarını tutan dosyaya işaret eden gösterici (pointer)
    char cwd[PATH_MAX]; // üzerinde çalışılan klasör adresini tutar (current working directory)
    char absolutefilename[PATH_MAX]; // tam yol ile beliritlen dosya ismi
    char codeline[MAX_LENGTH]; // SML dosyasından her bir satır okunurken karakterler bu diziye kaydedilir
    char temp[50];
    int storei_flag = 0; // hemen değer yükleme (immediate operation) 
                       // opcode 22 çalıştırılacağı zaman işlemciye bildirimde bulunan mantıksal bayrak
                       // (logical flag - bayraklar program akışını kontrol etmeyi sağlar)
    int reti_flag = 0; // hemen değer döndürme - opcode 51 çalıştırılacağı zaman işlemciye bildirimde bulunan mantıksal bayrak
    int addi_flag = 0; // hemen değer ekleme - opcode 36 çalıştırılacağı zaman işlemciye bildirimde bulunan mantıksal bayrak
    int subi_flag = 0; // hemen değer çıkarma - opcode 37 çalıştırılacağı zaman işlemciye bildirimde bulunan mantıksal bayrak
    int divi_flag = 0; // hemen değere bölme - opcode 38 çalıştırılacağı zaman işlemciye bildirimde bulunan mantıksal bayrak
    int muli_flag = 0; // hemen değerle çarpma - opcode 39 çalıştırılacağı zaman işlemciye bildirimde bulunan mantıksal bayrak
    int skip_opcode = 0; // yukarıdaki bayraklardan biri kalkınca bu bayrak da kalkar

    if (getcwd(cwd, sizeof(cwd)) != NULL) { // SML dosya adresine ulaş, 
                                            // SML.txt dosyası, üzerinde çalışılan proje dosyası (.vcxproj uzantılı) ile aynı klasörde bulundurulmalı

        sprintf(absolutefilename, "%s\\%s", cwd, SML_file);
        // printf("\n%s\n", absolutefilename);
        if ((file = fopen(absolutefilename, "r")) == NULL) { // SML dosyasını aç
            printf("\nTemin edilen SML dosyasi acilmiyor\n");
            return -1;
        }
        else {
            // SML dosyasının sonuna gelesiye kadar hafızadan komut-argüman çiftlerini oku ve hafızaya yaz
            // bunu, dosyadan her seferde bir satırı okuyarak yap
            // satır okuma işleminde komut-argüman çiftiyle beraber '%' karakteriyle başlayan yorumlar da okunur
            // ve codeline adlı karakter dizisne kaydedilir
            while (fgets(codeline, MAX_LENGTH, file)) {
                // printf("line %d: %s\n", i, codeline);
                char* pPtr = strchr(codeline, '%'); // SML dosyasında ilgili satırda '%' karakterinin bulunduğu konumu tespit et 
                                                    // ve pPtr isimli göstericiyle bu konuma işaret et
                char* sPtr = &codeline[0];
                int j = 0;
                while (sPtr != pPtr && *sPtr != '\0' && j < 50) { // codeline karakter dizisini '%' karakterine gelesiye kadar baştan sona tara ve 
                                       // sırayla elemanlarını temp dizisine aktar
                    temp[j] = *sPtr;
                    // printf("*sPtr: %c, j: %d\n", *sPtr, j);
                    sPtr++;  j++;
                }
                temp[j] = '\0';

                memory[i] = atof(temp); // karakter dizisinden sayıya dönüştürüp hafızaya yaz
                                       // opcode ve argüman (adres) 4 basamaklı tam sayısı 
                                       // veya adres içine doğrudan yüklenecek değer hafızaya yazıldı
                                       // bu değer ondalıklı sayı (float) tründe de olabilir ondan, hafızayı temsil
                                       // eden diziyi (memory) cinsinden seçtik. Eğer hafızaya yüklenecek sayı tams ayı ise
                                       // örn., opcode-argüman çifti, bunu tam sayı şeklinde ekrana yazdırırız (displayMemory fonksiyonu içinde)
                                       // Unutmayın ki, opcode-argüman çifti tam sayı olduğu halde bir ondalıklı sayı dizisi içinde tutlsa dahi
                                       // biz bunlarla halen gerekli işlemleri yapabiliriz. Çünkü, aslında tam sayılar ondadlıklı kısmı 0 olan
                                       // olan ondalıklı sayılar gibi yorumlanabilir (örn., 45 tam sayısı aslında 45.00 olarak da youmlanabilir).

                // temp[j-1] = '\0';
                // printf("temp: %s\n", temp);

                i++;
            }

            fclose(file); // SML dosyasını kapat
            i = 0;

            printf("\n\n****SML dosyasi hafizaya yuklendikten sonra hafizanin durumu****\n\n");
            displayMemory(memory, CODEMEMSIZE); // hafızaya yazılan komutları ekranda görüntüle

            while (memory[i] != 43000) { // hafızada bitirme (halt) komutuna gelinesiye kadar komutları sırayla çalıştır

                if (!storei_flag && !reti_flag && !addi_flag && !subi_flag && !divi_flag && !muli_flag) { // eğer kod hafızasından standart opcode-adres okuması yapılıyorsa
                    opcode = memory[i] / 1000; // çalıştırılacak SML dili komutu
                    address = (long long int)memory[i] % 1000; // bu komutun argümanı
                }
                else { // eğer kod hafızasından doğrudan bir adrese veya yazmaca (örn., akümülatör) yüklenecek değer okuması yapılıyorsa

                    if (storei_flag) { // hafızaya hemen bir değer yazılması (immediate operation)

                        if (address <= CODEMEMSIZE - 1) // kod hafızasından bir adrese hemen bir değer yazılacaksa
                            memory[address] = memory[i];
                        else { // eğer yazmaçlardan bir tanesine hemen bir değer yazılacaksa

                            if (address == ACCADDRESS) // akümülatörün adresi ise
                                accumulator = memory[i];
                            else if (INREGSTARTADDRESS <= address && address <= INREGENDADDRESS) // girdi yazmaçlarından birinin adresi ise
                                inreg[address - INREGSTARTADDRESS] = memory[i];
                        }
                        storei_flag = 0; // hafızaya doğrudan değer yazılması bayrağının sıfırlanması
                    }

                    if (reti_flag) { // hemen bir değer döndürülmesi
                        outreg[0] = memory[i];
                        reti_flag = 0; // hemen değer döndürülmesi bayrağının sıfırlanması
                    }

                    if (addi_flag) { // hemen bir değer eklenmesi
                        accumulator += memory[i];
                        addi_flag = 0; // hemen değer eklenmesi bayrağının sıfırlanması
                    }

                    if (subi_flag) { // hemen bir değer çıkarılması
                        accumulator -= memory[i];
                        subi_flag = 0; // hemen değer çıkarılması bayrağının sıfırlanması
                    }

                    if (divi_flag) { // hemen bir değere bölünmesi
                        accumulator /= memory[i];
                        divi_flag = 0; // hemen değere bölünmesi bayrağının sıfırlanması
                    }

                    if (muli_flag) { // hemen bir değerle çarpılması
                        accumulator *= memory[i];
                        muli_flag = 0; // hemen değerle çarpılması bayrağının sıfırlanması
                    }

                    skip_opcode = 1;
                }

                counter++;

                if (_DEBUG) {
                    printf("\nAccumulator : %.3lf", accumulator); // accumulator yazmacının o andaki değerini yazdırır
                    printf("\nOpcode      : %lld", opcode); // komutu yazdırır
                    printf("\nOperand     : %lld", address); // komutun argümanı olan adresi yazdırır
                    printf("\nCounter     : %d\n", counter); // sayacı yazdırır
                }

                if (!skip_opcode) { // eğer hemen işlem (immediate operation) yapılmıyorsa opcode çalıştırılır

                    switch (opcode) {

                    case 10: // klavyeden sayı girişi ve o sayının ilgili hafıza konumuna yerleştirilmesi
                        printf("Opcode degeri 10'dur, bu yuzden bir sayi girin: ");
                        scanf("%lld", &memory[address]);
                        break;
                    case 11: // ilgili hafıza konumundaki sayının ekrana yazdırılması
                        printf("\n%.3lf\n", memory[address]);
                        break;


                    case 20: // accumulator yazmacına ilgili hafıza konumundaki veya başka bir yazmaçtaki değeri yükleme
                        if (address <= CODEMEMSIZE - 1) // kod hafızasından bir adresteki değer yazılacaksa
                            accumulator = memory[address];
                        else { // eğer yazmaçlardan bir tanesinin değeri yazılacaksa

                            if (INREGSTARTADDRESS <= address && address <= INREGENDADDRESS) // girdi yazmaçlarından birinin adresi ise
                                accumulator = inreg[address - INREGSTARTADDRESS];
                        }
                        break;
                    case 21: // accumulator yazmacındaki değeri ilgili hafıza konumuna veya başka bir yazmaca yükleme
                        if (address <= CODEMEMSIZE - 1) // kod hafızasından bir adrese değer yazılacaksa
                            memory[address] = accumulator;
                        else { // eğer yazmaçlardan bir tanesine değer yazılacaksa

                            if (INREGSTARTADDRESS <= address && address <= INREGENDADDRESS) // girdi yazmaçlarından birinin adresi ise
                                inreg[address - INREGSTARTADDRESS] = accumulator;
                        }
                        break;

                    case 22: // ilgili hafıza konumuna veya bir yazmaca hemen bir değer yükleme (immediate operation)
                             // değer, SML dosyasında bu komutun yazıldığı satırın hemen ardındna gelen satırda yazılmaktadır
                             // dolayısıyla, bilgisayarın kod hafızasında 22 numaralı komutu içeren kelimenin (word) saklandığı
                             // adresten (SML dosyasındaki ilgili satırdan) hemen sonra gelen adres konumunda (SML dosyasında hemen sonr agelen satırda) saklanır
                             // bunun için, kod hafızasından çekilecek bir sonraki sayısal değerin bir komuttan ziyade yukarıda
                             // bahsedildiği gibi belirtilen bir değer olduğunu bilgisayarın anlaması adına 
                             // bir bayrağı kaldırırız (set the logical flag - bir yazılım deyimidir)
                        storei_flag = 1;
                        break;


                    case 30: // ilgili hafıza konumundaki veya yazmaçtaki değeri accumulator yazmacındaki değere ekleme ve sonucu yazmaçta saklama
                        if (address <= CODEMEMSIZE - 1) // kod hafızasından bir adresteki değer eklenecekse
                            accumulator += memory[address];
                        else { // eğer yazmaçlardan bir tanesinin değeri eklenecekse

                            if (address == ACCADDRESS) // akümülatörün adresi ise
                                accumulator += accumulator;
                            else if (INREGSTARTADDRESS <= address && address <= INREGENDADDRESS) // girdi yazmaçlarından birinin adresi ise
                                accumulator += inreg[address - INREGSTARTADDRESS];
                        }
                        break;
                    case 31: // ilgili hafıza konumundaki veya yazmaçtaki değeri accumulator yazmacındaki değerden çıkarma ve sonucu yazmaçta saklama
                        if (address <= CODEMEMSIZE - 1) // kod hafızasından bir adresteki değer çıkarılacaksa
                            accumulator -= memory[address];
                        else { // eğer yazmaçlardan bir tanesinin değeri çıkarılacaksa

                            if (address == ACCADDRESS) // akümülatörün adresi ise
                                accumulator -= accumulator;
                            else if (INREGSTARTADDRESS <= address && address <= INREGENDADDRESS) // girdi yazmaçlarından birinin adresi ise
                                accumulator -= inreg[address - INREGSTARTADDRESS];
                        }
                        break;
                    case 32: // accumulator yazmacındaki değeri ilgili hafıza konumundaki değere bölme ve sonucu yazmaçta saklama
                        if (address <= CODEMEMSIZE - 1) // kod hafızasından bir adresteki değere bölünecekse
                            accumulator /= memory[address];
                        else { // eğer yazmaçlardan bir tanesinin değerine bölünecekse

                            if (address == ACCADDRESS) // akümülatörün adresi ise
                                accumulator /= accumulator;
                            else if (INREGSTARTADDRESS <= address && address <= INREGENDADDRESS) // girdi yazmaçlarından birinin adresi ise
                                accumulator /= inreg[address - INREGSTARTADDRESS];
                        }
                        break;
                    case 33: // ilgili hafıza konumundaki veya yazmaçtaki değeri accumulator yazmacındaki değerle çarpma ve sonucu yazmaçta saklama
                        if (address <= CODEMEMSIZE - 1) // kod hafızasından bir adresteki değerle çarpılacaksa
                            accumulator *= memory[address];
                        else { // eğer yazmaçlardan bir tanesinin değeriyle çarpılacaksa

                            if (address == ACCADDRESS) // akümülatörün adresi ise
                                accumulator *= accumulator;
                            else if (INREGSTARTADDRESS <= address && address <= INREGENDADDRESS) // girdi yazmaçlarından birinin adresi ise
                                accumulator *= inreg[address - INREGSTARTADDRESS];
                        }
                        break;
                    case 34: // accumulator yazmacındaki değeri 1 arttır
                        accumulator += 1;
                        break;
                    case 35: // accumulator yazmacındaki değeri 1 azalt
                        accumulator -= 1;
                        break;

                    case 36: // accumulator yazmacındaki değere hemen bir değer ekleme (immediate operation) ve sonucu accumulatorde saklama
                        addi_flag = 1;
                        break;
                    case 37: // accumulator yazmacındaki değerden hemen bir değer çıkarma ve sonucu accumulatorde saklama
                        subi_flag = 1;
                        break;
                    case 38: // accumulator yazmacındaki değeri hemen bir değere bölme ve sonucu accumulatorde saklama
                        divi_flag = 1;
                        break;
                    case 39: // accumulator yazmacındaki değeri hemen bir değerle çarpma ve sonucu accumulatorde saklama
                        muli_flag = 1;
                        break;


                    case 40: // SML komutu 40 (dallanma) olduğunda ilgili adres argümanıyla belirtilen hafıza konumuna sıçrama
                        i = address;
                        continue;
                        break;
                    case 41: // accumulator yazmacındaki değer negatif ise ilgili adrese koşullu sıçrama
                        if (accumulator < 0) {
                            i = address;
                            continue;
                        }
                        break;
                    case 42: // accumulator yazmacındaki değer sıfır ise ilgili adrese koşullu sıçrama
                        if (accumulator == 0) {
                            i = address;
                            continue;
                        }
                        break;


                    case 50: // ilgili adresteki veya yazmaçtaki değeri döndürme - çıktı yazmacı (outreg) içine yazıldığında işlemci bu değeri döndürmüş olur
                        if (address <= CODEMEMSIZE - 1) // kod hafızasından bir adresteki değer döndürülücekse
                            outreg[0] = memory[address];
                        else { // eğer yazmaçlardan bir tanesinin değeri döndürülecekse
                            // printf("\nadres: %ld, inreg[0]: %ld, inreg[1]: %ld\n", address, (long long int)inreg[0], (long long int)inreg[1]);
                            if (address == ACCADDRESS) // akümülatörün adresi ise
                                outreg[0] = accumulator;
                            else if (INREGSTARTADDRESS <= address && address <= INREGENDADDRESS) // girdi yazmaçlarından birinin adresi ise
                                outreg[0] = inreg[address - INREGSTARTADDRESS];
                        }
                        break;
                    case 51: // hemen bir değer döndürme
                        reti_flag = 1;
                        break;
                    } // switch (opcode) sonu

                } // if (!skip_opcode) sonu
                else
                    skip_opcode = 0; // bir sonraki opcode işlenebilsin diye bayrağı indir

                i++; // program sayacını bir arttırma - bir sonraki çalıştırılacak komutun hafızadaki konumunu belirtir

            } // while (memory[i] != 4300) sonu

            printf("\n\n****Bilgisayar islemini tamamladiktan sonra hafizanin durumu****\n\n");
            displayMemory(memory, 100);
            return 1;

        } // else sonu

    } // if (getcwd(cwd, sizeof(cwd)) != NULL) sonu
    else {
        printf("\nSML dosyasi adresine ulasilamiyor\n");
        return -1;
    }

} // int SML_sim(void) sonu
