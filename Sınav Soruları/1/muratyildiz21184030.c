/*************************************************************/
/*                                                           */
/*                  Written by                               */
/*                 Erdem Dilmen                              */
/*           Mechatronics Engineering                        */
/*              Pamukkale University                         */
/*                  1.11.2021                                */
/*                                                           */
/*************************************************************/

#include <stdio.h>
#define _USE_MATH_DEFINES   // pi sayısını kullanmak isterseniz bu gereklidir
                            // bu sayı, M_PI sembolik sabiti ile gösterilmektedir
#include <math.h>           // pi sayısının kullanılabilmesi için dahil edilir


// aşağıdaki sembolik sabit konsol ekranından girilebilecek
// en uzun karakter dizininin boyutunu verir
// kullanıcı en fazla SIZE-1 karakterlik bir giriş yapabilir
#define SIZE 6000


// FONKSİYON PROTOTİPLERİ

double sin_(double angle);       // sinus fonksiyonu
double cos_(double angle);       // cosinus fonksiyonu
double asin_(double value);    // ters sinus fonksiyonu
double acos_(double value);    // ters cosinus fonksiyonu
double exp_(double x);           // eksponensiyel fonksiyonu
double log_(double value);       // doğal logaritma fonksiyonu (eksponensiyel fonksiyonunun tersidir)
double sqrt_(double x);          // kare kök alma fonksiyonu
void shuffleArray_l(void* data_array, size_t array_size, int castingtype); // dizi elemanlarının yerini değiştirme fonksiyonu
void shuffleArray_s(void* data_array, size_t array_size, int castingtype); // dizi elemanlarının yerini değiştirme fonksiyonu
void encrypt(const float* original_text_n, float* encrypted_text, size_t text_size); // şifreleme fonksiyonu
void decrypt(const float* encrypted_text, float* decrypted_text_n, size_t text_size); // şifre çözme fonksiyonu
int strcmp_(const char* str1, const char* str2); // iki karakter dizisini karşılaştırır

//#define DEBUG_

// ANA FONKSİYON

int main(void)
{
#ifndef DEBUG_
    char c; // kullanıcının gireceği her bir karakteri tutar
    char original_text[SIZE];    // kullanıcı tarafından girilecek asıl mesajı tutar
    float original_text_n[SIZE];    // kullanıcı tarafından girilecek asıl mesajın ASCII kodları normalize edilmiş halini tutar
    float encrypted_text[SIZE]; // şifrelenmiş mesajı tutar
                                 // Şifrelenmiş metin, sonradan şifre çözme işleminde kullanılacaktır.
                                 // Karakter dizileri karakterlerin tam sayı olan ASCII kod karşılıklarını tutar.
                                 // Şifreleme algoritması ondalıklı sayı üretir, eğer ondalıklı sayıları tam sayıya
                                 // çevirip saklarsak ondalıklı kısmı kaybederiz ve şifre çözme algoritması ile
                                 // asıl metni değil ona yakın bir metni elde edebiliriz. 
                                 // Bu yüzden, şifrelenmiş mesajı float cinsinden belirttik.  
    char encrypted_text_s[SIZE]; // ekrana yazdırmak için karakter dizini olan şifrelenmiş mesaj
    float decrypted_text_n[SIZE];   // şifresi çözülmüş mesajın normalize haldeki değerlerini tutar tutar
    char decrypted_text[SIZE];   // şifresi çözülmüş mesajı ASCII kod olarak tutar
    int i = 0; // sayaç i'ye ilk değer ataması

    // kullanıcıdan şifrelenmesi istenen mesajı girmesini isteyin
    // bu mesaj bir karakter dizini (string) olacaktır
    // ve bu dizin: harf, rakam, matematiksel operatör ve bu gibi
    // bir çok farklı türde karakteri içerebilir
    puts("\nAsagiya sifrelenmesini istediginiz mesaji girebilirsiniz.\n\n"
        "NOT:\n\n"
        "Bu mesaj bir karakter dizini(string) olacaktir\n"
        "ve bu dizin; harf, rakam, sembol, matematiksel operator ve bu gibi\n"
        "bir cok farkli turde karakteri icerebilir.\n"
        "  *** TURKCE KARAKTER GIRMEYINIZ ***\n"
        "Klavyeden girebileciginiz karakterler https://www.asciitable.com/ adresinde verilmektedir.\n"
        "Girebileceginiz en uzun mesaj, SIZE-1 karakterlik olabilir.\n"
        "Daha uzun bir mesaj girerseniz, indisi SIZE-1 degerinden buyuk olan karakterler ihmal edilir.\n"
        "SIZE sembolik sabiti kaynak kodun basinda 1000 olarak belirlenmistir.\n\n"
        "-- Girisi SONLANDIRMAK ICIN --\n"
        "i)   once <Enter> tusuna\n"
        "ii)  ardindan <ctrl + z> tus kombinasyonuna\n"
        "iii) ve son olarak ise yeniden <Enter> tusuna basin\n\n"
        "Bahsi gecen bu uc asamali sonlandirma girisi, dosya sonu karakteri diye bilinen EOF (end of file) olmaktadir.\n"
        "Mesajiniz birden cok satirdan ibaret olabilir.\n"
        "Bir satir yazdiktan sonra alt satira gecmek icin <Enter> tusuna basmaktan cekinmeyin.\n"
        "Bu, scanf() fonksyonunda oldugu gibi klavyeden yapilan girisi sonlandirmayacaktir. Bu program,\n"
        "veri girisinin klavyeden ancak EOF karakteri girilmesi durumunda sonlandirilacagi sekilde hazirlanmistir.\n\n"
        "Simdi lutfen sifrelenmesini istediginiz mesaji girin:\n\n"
        "NOT: SIFRELENECEK MESAJ, BIR METIN DOSYASINDAN OKUNMAKTADIR.\n");

    // getchar fonksiyonu klavyeden girilen karakteri okur ve karşılığında onun ASCII kod değerini döndürür
    // eğer bunu %d biçim belirteciyle yazdırırsanız girilen karakterin ASCII kod karşılığını görürsünüz
    // eğer %c biçim belirteciyle yazdırır iseniz bu karakterin kendisini görürsünüz
    // simdi en fazla SIZE-1 adet karakter olacak şekilde, EOF karakteri girilesiye kadar
    // kullanıcının girdiği tüm karakterleri alalım ve asıl mesaj dizisinin içine karakterleri kaydedelim
    const char* file_name = "original_text_file.txt";
    // const char* file_name = "original_text_file.txt";
    /*
    char absolutepathtofile[100];
    getcwd(absolutepathtofile, 100);
    sprintf(absolutepathtofile, "%s\\Midterm\\%s", absolutepathtofile, file_name);
    */
    // printf("%s\n", absolutepathtofile);
    FILE* original_text_file_p = fopen(file_name, "r+"); // absolutepathtofile
    if (!original_text_file_p)
        printf("File open ERROR: File %s cannot be opened\n", file_name);
    else {
        char c;
        while ((c = fgetc(original_text_file_p)) != EOF) {
            original_text[i++] = c;
        }
        original_text[i] = '\0';  // karakter dizinini sonlandıran NULL karakteri (mesajınıza dahil değildir)
                                     // ekrana karakter dizini yazdırmak için gereklidir
                                    // her karakter dizini hafızada, sonunda NULL karakter bulunduracak şekilde saklanır

        // kullanıcını girdiği mesajı ekrana geri yazdıralım
    // klavyeden girilen mesajın doğru şekilde alındığının teyit edilmesi amaçlı
        puts("\nKullanici tarafindan girilen ASIL mesaj:");
        puts(original_text);

        // girilen mesajdaki her karaktere karşılık gelen ASCII kod değerlerini yazdıralım
        printf("\nNOT:\n\n"
            "Karakterler hafizada [0, 255] tam sayi araligindaki ASCII kod degerleriyle saklanir.\n"
            "Ornegin, sifreleme islemi bu degerler uzerinden yapilir.\n"
            "Bu degerlere https://www.asciitable.com/ adresinden erisebilirsiniz.\n\n"
            "Kullanicinin girdigi ASIL mesajdaki her bir karakterin ASCII kod karsiligi:\n");

        size_t text_size = i; // kullanıcının girdiği karakterleri tutan dizinin uzunluğu
        float min = 255, max = 0; // normalizasyon için gerekli, asin fonksiyonu 90'dan büyük açıların sinüs değeriyle
                                      // küçüklerinkini aynı açıya döndürüyor (örn., asin_(100) = asin_(80)), 
                                      // hatalı sonuç elde ediliyor, şifreleme öncesi
                                      // tüm ASCII kodları 0-90 aralığına çekilmeli
        for (i = 0; i < text_size; i++) {
            printf("%4u", original_text[i]);
            if (original_text[i] < min)
                min = original_text[i];
            if (original_text[i] > max)
                max = original_text[i];
        }
        
        // printf("\nmax: %lf\n", max);

        // şifreleme öncesi [0, 90] aralığına normalizasyon
        for (i = 0; i < text_size; i++) {
            original_text_n[i] = ((original_text[i] - min) / (max - min)) * 90;
        }

        // Şifreleme işlemi
        printf("\n\nSifreleme algoritmasi calistiriliyor..\n");
        encrypt(original_text_n, encrypted_text, text_size);
        printf("\nSIFRELENMIS mesaj:\n");
        for (i = 0; i < text_size; i++) {
            encrypted_text_s[i] = (unsigned int)encrypted_text[i];
        }
        encrypted_text_s[i] = '\0';
        puts(encrypted_text_s);

        printf("\nSifrelenmis mesaj (ASCII kodu):\n");
        for (i = 0; i < text_size; i++) {
            printf("%4u", (unsigned int)encrypted_text[i]);
        }

        // Şifre çözme işlemi
        printf("\n\nSimdi, sifrelenmis mesaj uzerinde sifreyi cozme algoritmasi calistiriliyor..\n");
        decrypt(encrypted_text, decrypted_text_n, text_size);

        // decrypted_text_n, normalize edilmiş şekilde şifresi çözülmüş değerleri tutar. Denormalize edilmeli.
        // ardından, tam sayı değere yuvarlanıp ASCII kod olarak saklanmalı
        for (i = 0; i < text_size; i++) {
            float temp = (decrypted_text_n[i] * (max - min)) / 90 + min;
            // temp, denormalize edilmiş odnalıklı değer tutar
            // Ancak, ASCII kodlar tam sayıdır.
            // Bazen fonskiyonlar örneğin 43 döndürmesi gerekirken 42.9999 döndürmektedir. 
            // Ya da 43.0001 de döndürebilmektedir.
            // Böyle olanları en yakın tam sayıya yuvarlamamız gerekli.
            unsigned int intpart = (unsigned int)temp;
            if (temp - intpart <= 0.5) {
                decrypted_text[i] = intpart;
            }
            else {
                decrypted_text[i] = intpart + 1;
            }
        }
        
        printf("\nSIFRESI COZULMUS mesaj:\n");
        decrypted_text[text_size] = '\0';
        puts(decrypted_text);
        printf("\nSifresi cozulmus mesaj (ASCII kodu):\n");
        for (i = 0; i < text_size; i++) {
            printf("%4u", decrypted_text[i]);
        }

        /*for (i = 0; i <= text_size; i += 15) {
            decrypted_text[i] += 1;
        }*/

        printf("\n\n");

        // ASIL mesaj ile ŞİFRESİ ÇÖZÜLMÜŞ mesajı karşılaştır
        // Eğer her bir karakter karşılıklı olarak AYNI ise başarılı
        // DEĞİL ise BAŞARISIZ demektir
        int errcnt = strcmp_(original_text, decrypted_text);
        float student_score; // öğrencinin notu
        if (!errcnt) {
            printf("\t\tTEBRIKLER!\n"
                "ASIL metin ile SIFRESI COZULMUS metin AYNIDIR.\n"
                "SIFRELEME ve SIFRE COZME algoritmaniz DUZGUN calismaktadir.\n");
            student_score = 100;
        }
        else {
            float errprcnt = 100 * errcnt / text_size;
            printf("\t\tHATALI SONUC!\n"
                "ASIL metin ile SIFRESI COZULMUS metin ayni DEGILDIR.\n"
                "SIFRELEME ve SIFRE COZME algortimaniz YANLIS calismaktadir.\n"
                "Toplam %d karakterden %d adedi farkli cikmistir.\n"
                "Hata yuzdesi: %% %f\n", text_size, errcnt, errprcnt);
            if (errprcnt >= 10) {
                student_score = 0;
            }
            else {
                student_score = (10 - errprcnt) * 10;
            }
        }

        puts("");

        printf("\tOGRENCI PUANI: %f\n\n", student_score);
    }
#else
    // FONKSİYONLARINIZI AŞAĞIDAKİ KÜÇÜK KOD PARÇALARIYLA TEST EDEBİLİRSİNİZ
    // Emin olduktan sonra karakter dizilerini şifrelemeye geçin. Her fonksiyonun testi için 
    // ona karşılık geln kod bloğundaki '/* */' yorum işaretlerini kaldırın ve kodu açın. Örn., sinus ve ters sinus
    // fonkisyonunun testi için satır 232 ve 239'daki /* */ işaretlerini kaldırın ve böylece kod, yorumdan olamktan
    // kurtulup açılacak, çalıştırılabilecektir

    // sinus ve ters sinus fonksiyonlarının testi için
    /*
    // aşağıda angle_d değişkenine [0, 180] derece arasında farklı değerler verip test edin
    double angle_d = 89; // derece cinsinden açı
    double angle_r = angle_d * (2 * M_PI / 360);
    double sinval = sin_(angle_r);
    printf("angle: %.10lf => sin(angle): %.10lf\n", angle_d, sinval);
    printf("val=sin(angle): %.10lf => angle in degrees=asin(val): %.10lf\n", sinval, (360 / (2 * M_PI)) * asin_(sinval));
    */

    // eksponensiyel ve doğal logaritma (log) fonksiyonlarının testi için
    /*
    // aşağıda x'e farklı değerler verip test edin 
    double x = 4;  // ara sınav için yazılan fonksiyon en fazla 10 değeri alır, sınav ile uyumlu
    double expval = exp_(x);
    printf("x: %lf => exp(x): %lf\n", x, expval);
    printf("val=exp(x): %lf => x=log(val): %lf\n", expval, log_(expval));
    */

    // kare kök alma fonksiyonunun testi için
    /*
    // aşağıda x'e farklı değerler verip test edin
    double x =9.57;
    double sqrval = x * x;
    printf("x: %lf => x^2: %lf\n", x, x * x);
    printf("val=x^2: %lf => x=sqrt(x^2): %lf", sqrval, sqrt_(sqrval));
    */

    // dizi elemanlarının yerini değiştirme fonksiyonunun testi için
    /*
    // aşağıdaki 4 farklı deneme metni ile deneyiniz (birini denerken diğerlerini yorum yapıp kapatınız)
    char test_text[SIZE] = "KalemKitapTahtaDolap";
    // char test_text[SIZE] = "KalemKitap1TahtaDolap";
    // char test_text[SIZE] = "Kalem1KitapTahta2Dolap";
    //char test_text[SIZE] = "Kalem1Kitap3Tahta2Dolap";
    size_t test_text_size = 0; // deneme metnindeki karakter sayısı
    // asıl deneme metninin bir kopyasını oluşturalım
    char test_text_cp[SIZE];
    while ((test_text_cp[test_text_size] = test_text[test_text_size]) != '\0') {
        // NULL karaktere kadar okur,
        // dizideki toplam karakter sayısını 'test_text_size' içinde tutar (dizi boyutu)
        // ve kopyalama işlemi yapar
        test_text_size++;
    }
    // önce deneme metninin ekrana yazdıralım
    printf("\nshuffleArray fonksiyonunun testi:\n"
        "Deneme metni asagidadir:\n\n");
    puts(test_text);
    // ardından bunun elemanlarının yerlerini değiştirelim
    shuffleArray_l(test_text_cp, test_text_size, 2); // karakter dizisi ile çalıştığımız için 3. argüman 2 olmalı
    // ve ekrana elemanlarının yerleri değiştirilmiş metni yazalım
    printf("\nDeneme metni shuffleArray fonksiyonundan gecirildikten sonra:\n");
    puts(test_text_cp);
    puts("\nDIKKAT! YUKARIDAKI METIN OLMASI GEREKTIGI GIBI MI? KONTROL EDIN.");
    // aynı fonksiyondan bir daha geçirirsek eski haline dönmeli
    // eğer ilk metin ile aynısını ekranda okuyorsanız başarılı demektir
    // değil ise kodunuzu gözden geçiriniz
    shuffleArray_s(test_text_cp, test_text_size, 2);
    printf("\nDeneme metni TEKRAR shuffleArray fonksiyonundan gecirildikten sonra:\n");
    puts(test_text_cp);
    if (!strcmp_(test_text, test_text_cp)) {
        printf("\n\t\tTEBRIKLER!\n"
            "shuffleArray fonksiyonunuz DUZGUN calismaktadir.\n");
    }
    else {
        printf("\n\t\t\HATALI SONUC!\n"
            "shuffleArray fonksiyonunuz YANLIS calismaktadir.\n");
    }
    */
    
#endif // DEBUG_
}


// FONKSİYON TANIMLARI

// Sinus fonksiyonunun tanımı:
// Sinus fonksiyonunu, Taylor açılımından istifade ederek yaklaşık olarak ifade eder.
// Açılımdaki kesirli ifadelerin sayısı çözünrlüğü verir ve fonksiyon gövdesinde 'resolution' adlı
// bir tam sayı değişkeni ile ifade edilir. Bu sayıya, fonksiyonun istenilene yakın, kabul edilebilir
// bir değer döndürmesini sağlayacak kadar büyük bir değer verilmelidir.
// @input: açı değeri (radyan cinsinden) ondalıklı sayı
// @return: sinus değeri [-1, +1] aralığında ondalıklı sayı
double sin_(double angle) {

    int resolution = 15; // Fonksiyonun BAŞARIMI üzerine DOĞRUDAN ETKİSİ vardır! YÜKSEK değer girin - 15
    int counter_res = 1; // çözünürlük sayacı
    double n = 0; // sinus hesaplamasında paydada kullanılan ve iki katının bir fazlasınınfaktöriyeli alınan sayı
    int coeff = -1; // sinus hesaplamasında -1 üzeri n için kullanılır
    double term = angle; // özyinelemeli olarak hesaplanan terim
    double sin_computed = term; // hesaplanan sinus değeri

    // sinusü çözünürlük kadar adımda hesapla
    while (counter_res++ <= resolution) {


        term *= (angle*angle)/ ((2*n+3) * (2*n+2));
        sin_computed += coeff * term;

      coeff *=-1;

      n++;

    }
    
    
    // sinus değeri [-1, +1] aralığında olmalıdır, küsüratla da olsa hesap hatası olduya düzeltilsin
    if (sin_computed < -1)
        sin_computed = -1;
    if (sin_computed > 1)
        sin_computed = 1;
    

    //printf("sin_ value: %lf\n", sin_computed);

    return sin_computed; // hesaplanmış sinus değeri döndürülür
}

// Ters sinus fonksiyonunun tanımı:
// Ters sinus fonksiyonunu, Taylor açılımından istifade ederek yaklaşık olarak ifade eder.
// @input: sinus değeri [-1, +1] aralığında ondalıklı sayı
// @return: açı değeri (radyan cinsinden)
double asin_(double value) {

    double asin_computed; // hesaplanan ters sinus değeri

    // -90 ve +90 değerlerinde tam yakınsayamıyor
    if (value == 1) {
        asin_computed = M_PI_2;
    }
    else if (value == -1) {
        asin_computed = - M_PI_2;
    }
    else {
        int resolution = 100000; // Fonksiyonun BAŞARIMI üzerine DOĞRUDAN ETKİSİ vardır! YÜKSEK değer girin - 100000
        int counter_res = 1; // çözünürlük sayacı
        double n = 1; // ters sinus hesaplamasında kullanılan indis
        double term = value;
        asin_computed = term;

        // ters sinüsü çözünürlük kadar adımda hesapla
        while (counter_res++ <= resolution) {
            
            term *= (((2*n-1) * (2*(n-1)+1))/((2*n) * (2*n+1)))*(value*value);
            asin_computed += term;
            n++;

        }
    }

    // printf("asin_ value: %lf\n", asin_computed);
    return asin_computed; // hesaplanmış ters sinus değeri döndürülür
}

// Eksponensyel fonksiyonunun tanımı:
// Eksponensiyel fonksiyonunu, Taylor açılımından istifade ederek yaklaşık olarak ifade eder.
// Açılımdaki kesirli ifadelerin sayısı çözünrlüğü verir ve fonksiyon gövdesinde 'resolution' adlı
// bir tam sayı değişkeni ile ifade edilir. Bu sayıya, fonksiyonun istenilene yakın, kabul edilebilir
// bir değer döndürmesini sağlayacak kadar büyük bir değer verilmelidir.
// @input - x: eksponensiyel fonksiyonu bulunacak ondalıklı sayı
// @return: eksponensiyel fonksiyonu değeri - ondalıklı sayı
double exp_(double x) {

    int resolution = 400; // Fonksiyonun BAŞARIMI üzerine DOĞRUDAN ETKİSİ vardır! YÜKSEK değer girin 
    int counter_res = 1; // çözünürlük sayacı
    double n = 0; // eksponensiyel hesaplamasında kullanılan indis
    double term = 1;
    double e_pow_x_computed = term; // hesaplanan eksponensiyel fonksiyon değeri

    // eksponensiyeli çözünürlük kadar adımda hesapla
    while (counter_res++ <= resolution) {

        int val = 0;
        double fact = 1;
        while (val <= n - 1) {
            // printf("\nval: %d", val);
            if (n - val > 0)
                fact *= (n - val);
            val++;
        }
        // factoriel hesabı tamamlanır - faktöriyel, paydada kullanılır
        // printf("fact: %lf\n", fact);

        term += (1 / fact); // e sayısına çözünürlük döngüsünde ilgili adımından katkı

        double nom = 1; // eksponensiyel hesaplamasında payda kullanılan 
                        // terim: x'in kuvvetleri
        int counter_nom = 1; // pay için sayaç
        while (counter_nom++ <= n) {
            // printf("counter_num: %d, n: %d\n", counter_num, n);
            nom *= x;
            // printf("num: %lf, x: %lf, n: %d\n", num, x, n);
        }
        // printf("num / fact: %lf, num: %lf, fact: %lf\n", num / fact, num, fact);

        e_pow_x_computed += nom / fact; // eksponensiyel fonksiyonuna çözünürlük döngüsünde
                                        // ilgili adımından katkı
        n++;

        if(counter_res==2) e_pow_x_computed = e_pow_x_computed-1;

    }
    // printf("\ncomputed e(x): %lf", e_pow_x_computed);

    return e_pow_x_computed;
}

// Doğal logaritma fonksiyonunun tanımı:
// Doğal logaritma fonksiyonunu, açılımından istifade ederek yaklaşık olarak ifade eder.
// Açılımdaki kesirli ifadelerin sayısı çözünrlüğü verir ve fonksiyon gövdesinde 'resolution' adlı
// bir tam sayı değişkeni ile ifade edilir. Bu sayıya, fonksiyonun istenilene yakın, kabul edilebilir
// bir değer döndürmesini sağlayacak kadar büyük bir değer verilmelidir.
// @input - value: doğal logaritması alınacak ondalıklı sayı
// @return: hesaplanmış doğal logaritma fonksiyonu değeri - ondalıklı sayı
double log_(double value) {

    int resolution = 30; // Fonksiyonun BAŞARIMI üzerine DOĞRUDAN ETKİSİ vardır! YÜKSEK değer girin
    int counter_res = 1; // çözünürlük sayacı 
    double log_computed; // hesaplanan logaritma değeri

    // Standart log fonksiynu 10 değerine kadar düzgün çalışıyor ancak daha yüksek sayılarda ise bunları
    // değeri 10'a kadar olan küçük sayıların cinsinden ifade edersek başarabiliriz.
    // Önce sayıyı şu biçimde yazalım.
    // value = mul * 10 ^ pow = mul * powten (10'un bir kuvveti ile başka bir sayının çarpımı şeklinde,
    // örn., 350 = 3.5 * 10 ^ 2  --> ^ işareti üs alma anlamına gelir
    // bu örnekte mul = 3.5, pow = 2 ve powten = 100 olmaktadır)
    double mul; // girdiyi 10'un kuvveti cinsinden yazınca 10'un kuvveti ile çarpım halinde olan sayı
    int pow = 0; // girdiyi 10'un kuvveti cinsinden yazınca 10'un üssünde yer alan sayı (örn., 10 ^ x'teki x)
    double powten = 1; // girdiyi 10'un kuvveti cinsinden yazınca 10'un üssünün değeri (örn., 10 ^ x değeri)
    double temp = value;
    // printf("temp: %lf\n", temp);
    
    // aşağıdaki while bloğu, girdinin 10'un kaçıncı olduğunu bulmak için gereklidir
    while ((unsigned long long int)temp > 0) {
        
                if( (temp /= 10) > 1){
            powten *= 10;
            pow++;
        }

    }
    mul = value / powten;
    double log10 = 2.3025850929940457;  // value = mul * 10 ^ pow şeklnde yazıldığında
                                        // log(value) = log(mul) + pow * log(10) olur
                                        // burada log(10) = log10 değişkeninin değeridir
                                        // O halde, biz sadece mul değişkeninin logaritmasını hesaplamalıyız

    // printf("mul: %lf, pow: %d, powten: %lf\n", mul, pow, powten);

    // 'mul' değişkeni için logaritmayı hesapla
    double n = 2; // logaritma hesaplamasında kullanılan indis
    double num = (mul - 1) / (mul + 1);
    double term = num;
    double log_computed_mul = term; // 'mul' için hesaplanan logaritma değeri

    // logaritmayı çözünürlük kadar adımda hesapla
    while (counter_res++ <= resolution) {
        
        double numerator = 2*(n-1)-1;
        double denominator = 2*n-1;
        term *= (num*num) * (numerator/denominator);
        log_computed_mul += term;
        n++;

    }
    log_computed_mul *= 2;      // mul değişkeni için logaritma hesabı tamamlanır

    // şimdi asıl girdimiz olan 'value' için logaritmayı hesaplayalım
    log_computed = log_computed_mul + pow * log10; // logaritma hesabı tamamlanır

    return log_computed; // hesaplanan logaritma değeri döndürülür
}

// Kare kök alma fonksiyonunun tanımı:
// Kare kök alma fonksiyonunu, açılımından istifade ederek yaklaşık olarak ifade eder.
// Açılım adım adım ilerleyerek gerçek kare köke yakınsar. Adımların sayısı çözünrlüğü verir ve fonksiyon gövdesinde 'resolution' adlı
// bir tam sayı değişkeni ile ifade edilir. Bu sayıya, fonksiyonun istenilene yakın, kabul edilebilir
// bir değer döndürmesini sağlayacak kadar büyük bir değer verilmelidir.
// @input - x: kare kökü bulunacak ondalıklı sayı
// @return: hesaplanmış kare kök değeri - ondalıklı sayı
double sqrt_(double x) {

    double guess = 0;    // x'e en yakın kare kök tahmini
    double mrgn = 0.001; // algoritma adımlarında 0'a bölme işleminden kurtulmak için

    int resolution = 20; // Fonksiyonun BAŞARIMI üzerine DOĞRUDAN ETKİSİ vardır! YÜKSEK değer girin
    int counter_res = 1; // çözünürlük sayacı 

    // x'e en yakın kare kökü bulalım (binary search - ikili arama)
    int start = 0, end = (int)x;
    double mid; // arama için başlangıç, bitiş ve orta elemanlar
    while (start <= end) {
        
        // orta elemanı bulalım
        mid = (start + end) / 2;
        // eğer sayı tam kare ise bitir
        if (mid * mid == x) {
            guess = mid;
            break;
        }
        // eğer tam kare, orta elemandan büyük ise başlangıç elemanını arttır
        if (mid * mid < x) {
            // önce, başlangıç elemanı tahminimiz eklenmeli
            guess = start;
            // sonra, başlangıç elemanını güncelle
            start = mid + 1;
        }
        // eğer tam kare, orta elemandan küçük ise bitiş elemanını azalt
        else {
            end = mid - 1;
        }

    }
    // x' en yakın kare kök bulundu
    // printf("guess: %lf\n", guess);
    // şimdi adım adım ilerleyerek x'in gerçek kare köküne yakınsayalım
    double d;              // x ile en yakın kare tahmini arasındaki fark
    double P;              // ara değişken
    double A;              // ara değişken 
    double sqrt_comuted = guess;   // hesaplanan kare kök
    while (counter_res++ <= resolution) {
        
        d = x - sqrt_comuted * sqrt_comuted;    // x ile en yakın tam kare tahmini arasındaki fark
        P = d / (2 * sqrt_comuted + mrgn); // ara değişken
        A = sqrt_comuted + P;       // ara değişken 
        sqrt_comuted = A - ((P * P) / (2 * A));   // hesaplanmış kare kök her adımda güncellenir

    }

    return sqrt_comuted; // hesaplanmış kare kök döndürülür
}

// Dizi elemanlarının yerini değiştirme fonksiyonunun tanımı:
// Şifrelenecek/şifresi çözülecek metindeki karakterlerin yerlerini değiştirir.
// Herhangi bir türde eleman tutan dizi üzerinde işlem yapılabilmesi için değişken türü olarak
// void seçilmiştir. Fonksiyon gövdesinde, hangi türde değişkenle işlem yapılacaksa o türe 
// dönüşümü yapılır (type casting). 
// @input - data_array: elemanlarının yeri değiştirilecek dizinin başlangıç adresi
// @input - array_size: bu dizinin boyutu
// @input - castingtype: 1 ise float tipine, 2 ise char tipine dönüşüm yapılır
// @return - elemanarının yeri değiştirilmiş dizinin başlangıç adresi
void shuffleArray(void* data_array, size_t array_size, int castingtype) {

    // karakter dizimizin boyutu şu 4 olasılıktan birini sağlar (d: boyut, n ise bir tam sayı olsun)
    // karakter dizimizin boyutu şu 4 olasılıktan birini sağlar (d: boyut, n ise bir tam sayı olsun)
    // 1) d = 2 * (2 * n)           = 4 * n
    // 2) d = 2 * (2 * n)       + 1 = 4 * n + 1 
    // 3) d = 2 * (2 * n + 1)       = 4 * n + 2
    // 4) d = 2 * (2 * n + 1)   + 1 = 4 * n + 3
    // dizi boyutunun bunlardan hangisini sağladığını belirlemeliyiz ve sonrasında
    // diziyi dört alt dizi olarak ele almalıyız
    int n = array_size / 4;
    int quartxstart[4] = { 0 };  // dizinin x numaralı dörtlüğünün başlangıç indisi, x = 1, 2, 3, 4
    int quartxend[4];            // dizinin x numaralı dörtlüğünün bitiş indisi, x = 1, 2, 3, 4
    switch (array_size % 4) {

        // BURAYI DOLDURUN - AŞAĞIDAKİ İNDİS DEĞERELERİNİ DOLDURUN
        // BAŞLANGIÇ OLARAK 0 GİRİLMİŞTİR ANCAK SİZ BUNLARI DOĞRU DEĞERLERLE YER DEĞİŞTİRMELİSİNİZ

    case 0:     // d = 4 * n durumu
        quartxstart[1] = n; quartxstart[2] = 2 * n; quartxstart[3] = 3 * n;
        quartxend[0] = n - 1;  quartxend[1] = 2 * n - 1; quartxend[2] = 3 * n - 1; quartxend[3] = 4 * n - 1;
        break;
    case 1:     // d = 4 * n + 1 durumu
        quartxstart[1] = n; quartxstart[2] = 2 * n + 1; quartxstart[3] = 3 * n + 1;
        quartxend[0] = n - 1;  quartxend[1] = 2 * n - 1; quartxend[2] = 3 * n; quartxend[3] = 4 * n;
        break;
    case 2:     // d = 4 * n + 2 durumu
        quartxstart[1] = n + 1; quartxstart[2] = 2 * n + 1; quartxstart[3] = 3 * n + 2;
        quartxend[0] = n - 1;  quartxend[1] = 2 * n; quartxend[2] = 3 * n; quartxend[3] = 4 * n + 1;
        break;
    case 3:     // d = 4 * n + 3 durumu
        quartxstart[1] = n + 1; quartxstart[2] = 2 * n + 2; quartxstart[3] = 3 * n + 3;
        quartxend[0] = n - 1;  quartxend[1] = 2 * n; quartxend[2] = 3 * n + 1; quartxend[3] = 4 * n + 2;
        break;
    }
    // şimdi 4 alt dizide karşılıklı gelenler arasında eleman değiştirme yapalım
    // ilk yarıdaki iki alt dizi (1. ve 2. dörtlük) ile 
    // ikinci yarıdaki iki alt dizi (3. ve 4. dörtlük) arasında yer değiştirme yapılır
    for (int i = 0; i <= 1; i++) {
        // arasında eleman değişimi gerçekleştirilecek iki adet alt dizinin toplam 4 adet alt dizi
        // içindeki indisleri
        int firstarrayind = 2 * i, secondarrayind = 2 * i + 1;
        // ilgili iki alt dizi arasında eleman değiştirme başlar
        for (int j = 0; j < n; j++) {
            if (castingtype == 1) {
                // eleman yerleştirmeden hemen önce gerekli tür dönüşümü yapılır 
                // aşağıda tanımlı 'myarray', artık elemanlarının yerini değiştirdiğimiz diziyi (data_array) göstermektedir
                // onun üzerinde yaptığımız değişiklikler doğrudan 'data_array' üzerinde yapılmış olur.
                // castingtype = 1 => float
                /*
                float* myarray = (float*)data_array;
                float temp;
                if(j==0) temp = myarray[quartxstart[firstarrayind] + j];
                myarray[quartxstart[firstarrayind] + j] = myarray[quartxend[secondarrayind] - j];
                myarray[quartxend[secondarrayind] - j] = temp;
                */
                float* myarray = (float*)data_array;
                float temp = myarray[quartxstart[firstarrayind] + j];
                myarray[quartxstart[firstarrayind] + j] = myarray[quartxend[secondarrayind] - j];
                myarray[quartxend[secondarrayind] - j] = temp;

            }
            else {
                // castingtype = 2 = > char
                /*
                char* myarray = (char*)data_array;
                char temp;
                if(j==0) temp = myarray[quartxstart[firstarrayind] + j];
                myarray[quartxstart[firstarrayind] + j] = myarray[quartxend[secondarrayind] - j];
                myarray[quartxend[secondarrayind] - j] = temp;
                */
                char* myarray = (char*)data_array;
                char temp = myarray[quartxstart[firstarrayind] + j];
                myarray[quartxstart[firstarrayind] + j] = myarray[quartxend[secondarrayind] - j];
                myarray[quartxend[secondarrayind] - j] = temp;

            }

        }
    }
}

// Şifreleme fonksiyonunun tanımı:
// Şifrelenecek metindeki karakterlerin ASCII kod karşılıklarını cosinus fonksiyonundan geçirir.
// @input - original_text: şifrelenecek metin
// @input - encrypted_text: şifrelenmiş metin
// @input - text_size: metindeki karakter sayısı 
void encrypt(const float* original_text_n, float* encrypted_text, size_t text_size) {

    // önce, her bir karaktere şifreleme işlemi uygulayalım
    for (size_t i = 0; i < text_size; i++) {
        float temp = exp_(1 + sin_((2 * M_PI / 360) * original_text_n[i]));
        encrypted_text[i] = 3 * (temp * temp);

        // printf("encrypted_text[%d]: %.10lf\n", i, encrypted_text[i]);
    }
    // sonra, karakterlerin yerlerini değiştirelim
    shuffleArray(encrypted_text, text_size, 1);
    // şifreleme TAMAM!
}

// Şifre çözme fonksiyonunun tanımı:
// Şifrelenmiş metindeki karakterlerin sayısal değerlerini şifre çözme fonksiyonundan geçirir.
// @input - encrypted_text: şifrelenmiş metin
// @input - decrypted_text: şifresi çözülmüş metin
// @input - text_size: metindeki karakter sayısı 
void decrypt(const float* encrypted_text, float* decrypted_text_n, size_t text_size) {

    // önce, her bir karaktere şifre çözme işlemi uygulayalım
    for (size_t i = 0; i < text_size; i++) {
        // normalize edilmiş bir şekilde şifresi çözülmüş değer elde edilir
        // sonra, normalizayonun yapıldığı yerde denormalize edilmesi gerekli
        
        decrypted_text_n[i] = (360 / (2 * M_PI)) * asin_(log_(sqrt_((double)encrypted_text[i] / 3)) - 1);
        // printf("decrypted_text[%d] as floating point number: %.10lf\n", i, temp);
    }
    // sonra, karakterlerin yerlerini değiştirelim ve eski haline getirelim
    shuffleArray(decrypted_text_n, text_size, 1);
    // şifre çözme TAMAM!
}

// Karakter dizilerini karşılaştırma fonksiyonunun tanımı:
// İki adet karakter dizisini karşılaştırır. Her iki dizi de aynı boyutta olmalıdır.
// İlk dizide NULL karakterlerle karşılaşasıya kadar karakterleri okur.
// @input - str1: karşılaştırılacak karakter dizisi
// @input - str2: karşılaştırılacak diğer karakter dizisi
// @return      : karşılaştırma soncunda farklı çıkan karakter sayısı
//                      (eğer sonuç sıfır ise iki dizi bir biriyle aynıdır)
int strcmp_(const char* str1, const char* str2) {

    int i = 0, err = 0;
    while (str1[i] != '\0') {
        if (str1[i] != str2[i]) {
            err++;
           // printf("original: %d, decrypted: %d\n", str1[i], str2[i]);
        }
        i++;
    }
    return err; // aynı olmayan karakter sayısı
}