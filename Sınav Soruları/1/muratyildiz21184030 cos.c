/*************************************************************/
/*                                                           */
/*                  Written by                               */
/*                 Erdem Dilmen                              */
/*           Mechatronics Engineering                        */
/*              Pamukkale University                         */
/*                  1.11.2021                               */
/*                                                           */
/*************************************************************/

#include <stdio.h>
#define _USE_MATH_DEFINES   // pi sayısını kullanmak isterseniz bu gereklidir
                            // bu sayı, M_PI sembolik sabiti ile gösterilmektedir
#include <math.h>           // pi sayısının kullanılabilmesi için dahil edilir


// aşağıdaki sembolik sabit konsol ekranından girilebilecek
// en uzun karakter dizininin boyutunu verir
// kullanıcı en fazla SIZE-1 karakterlik bir giriş yapabilir
#define SIZE 5000


// FONKSİYON PROTOTİPLERİ

double cos_(double angle);       // cosinus fonksiyonu
double arccos_(double value);    // ters cosinus fonksiyonu
double exp_(double x);           // eksponensiyel fonksiyonu
double log_(double value);       // doğal logaritma fonksiyonu (eksponensiyel fonksiyonunun tersidir)
double sqrt_(double x);          // kare kök alma fonksiyonu
void shuffleArray_l(void* data_array, size_t array_size, int castingtype); // dizi elemanlarının yerini değiştirme fonksiyonu
void shuffleArray_s(void* data_array, size_t array_size, int castingtype); // dizi elemanlarının yerini değiştirme fonksiyonu
void encrypt(const char* original_text, float* encrypted_text, size_t text_size); // şifreleme fonksiyonu
void decrypt(const float* encrypted_text, char* decrypted_text, size_t text_size); // şifre çözme fonksiyonu
int strcmp_(const char* str1, const char* str2); // iki karakter dizisini karşılaştırır


// ANA FONKSİYON

int main(void)
{
    char c; // kullanıcının gireceği her bir karakteri tutar
    char orginial_text[SIZE];    // kullanıcı tarafından girilecek asıl mesajı tutar
    float encrypted_text[SIZE]; // şifrelenmiş mesajı tutar
                                 // Şifrelenmiş metin, sonradan şifre çözme işleminde kullanılacaktır.
                                 // Karakter dizileri karakterlerin tam sayı olan ASCII kod karşılıklarını tutar.
                                 // Şifreleme algoritması ondalıklı sayı üretir, eğer ondalıklı sayıları tam sayıya
                                 // çevirip saklarsak ondalıklı kısmı kaybederiz ve şifre çözme algoritması ile
                                 // asıl metni değil ona yakın bir metni elde edebiliriz. 
                                 // Bu yüzden, şifrelenmiş mesajı float cinsinden belirttik.  
    char encrypted_text_s[SIZE]; // ekrana yazdırmak için karakter dizini olan şifrelenmiş mesaj
    char decrypted_text[SIZE];   // şifresi çözülmüş mesajı tutar
    int i = 0; // sayaç i'ye ilk değer ataması

    // kullanıcıdan şifrelenmesi istenen mesajı girmesini isteyin
    // bu mesaj bir karakter dizini (string) olacaktır
    // ve bu dizin: harf, rakam, matematiksel operatör ve bu gibi
    // bir çok farklı türde karakteri içerebilir
    puts("\nAsagiya sifrelenmesini istediginiz mesaji girebilirsiniz.\n\n"
        "NOT:\n\n"
        "Bu mesaj bir karakter dizini(string) olacaktir\n"
        "ve bu dizin; harf, rakam, sembol, matematiksel operator ve bu gibi\n"
        "bir cok farkli turde karakteri icerebilir.\n"
        "  *** TURKCE KARAKTER GIRMEYINIZ ***\n"
        "Klavyeden girebileciginiz karakterler https://www.asciitable.com/ adresinde verilmektedir.\n"
        "Girebileceginiz en uzun mesaj, SIZE-1 karakterlik olabilir.\n"
        "Daha uzun bir mesaj girerseniz, indisi SIZE-1 degerinden buyuk olan karakterler ihmal edilir.\n"
        "SIZE sembolik sabiti kaynak kodun basinda 1000 olarak belirlenmistir.\n\n"
        "-- Girisi SONLANDIRMAK ICIN --\n"
        "i)   once <Enter> tusuna\n"
        "ii)  ardindan <ctrl + z> tus kombinasyonuna\n"
        "iii) ve son olarak ise yeniden <Enter> tusuna basin\n\n"
        "Bahsi gecen bu uc asamali sonlandirma girisi, dosya sonu karakteri diye bilinen EOF (end of file) olmaktadir.\n"
        "Mesajiniz birden cok satirdan ibaret olabilir.\n"
        "Bir satir yazdiktan sonra alt satira gecmek icin <Enter> tusuna basmaktan cekinmeyin.\n"
        "Bu, scanf() fonksyonunda oldugu gibi klavyeden yapilan girisi sonlandirmayacaktir. Bu program,\n"
        "veri girisinin klavyeden ancak EOF karakteri girilmesi durumunda sonlandirilacagi sekilde hazirlanmistir.\n\n"
        "Simdi lutfen sifrelenmesini istediginiz mesaji girin:\n\n"
        "NOT: SIFRELENECEK MESAJ, BIR METIN DOSYASINDAN OKUNMAKTADIR.\n");

    // getchar fonksiyonu klavyeden girilen karakteri okur ve karşılığında onun ASCII kod değerini döndürür
    // eğer bunu %d biçim belirteciyle yazdırırsanız girilen karakterin ASCII kod karşılığını görürsünüz
    // eğer %c biçim belirteciyle yazdırır iseniz bu karakterin kendisini görürsünüz
    // simdi en fazla SIZE-1 adet karakter olacak şekilde, EOF karakteri girilesiye kadar
    // kullanıcının girdiği tüm karakterleri alalım ve asıl mesaj dizisinin içine karakterleri kaydedelim
    const char* file_name = "deneme.txt";
    // const char* file_name = "original_text_file.txt";
    /*
    char absolutepathtofile[100];
    getcwd(absolutepathtofile, 100);
    sprintf(absolutepathtofile, "%s\\Midterm\\%s", absolutepathtofile, file_name);
    */
    // printf("%s\n", absolutepathtofile);
    FILE* original_text_file_p = fopen(file_name, "r+"); // absolutepathtofile
    if (!original_text_file_p)
        printf("File open ERROR: File %s cannot be opened\n", file_name);
    else {
        char c;
        while ((c = fgetc(original_text_file_p)) != EOF) {
            orginial_text[i++] = c;
        }
        orginial_text[i] = '\0';  // karakter dizinini sonlandıran NULL karakteri (mesajınıza dahil değildir)
                                     // ekrana karakter dizini yazdırmak için gereklidir
                                    // her karakter dizini hafızada, sonunda NULL karakter bulunduracak şekilde saklanır
        
        // kullanıcını girdiği mesajı ekrana geri yazdıralım
    // klavyeden girilen mesajın doğru şekilde alındığının teyit edilmesi amaçlı
        puts("\nKullanici tarafindan girilen ASIL mesaj:");
        puts(orginial_text);

        // girilen mesajdaki her karaktere karşılık gelen ASCII kod değerlerini yazdıralım
        printf("\nNOT:\n\n"
            "Karakterler hafizada [0, 255] tam sayi araligindaki ASCII kod degerleriyle saklanir.\n"
            "Ornegin, sifreleme islemi bu degerler uzerinden yapilir.\n"
            "Bu degerlere https://www.asciitable.com/ adresinden erisebilirsiniz.\n\n"
            "Kullanicinin girdigi ASIL mesajdaki her bir karakterin ASCII kod karsiligi:\n");

        size_t text_size = i - 1; // kullanıcının girdiği karakterleri tutan dizinin uzunluğu
        for (i = 0; i <= text_size; i++) {
            printf("%4u", orginial_text[i]);
        }

        // Şifreleme işlemi
        printf("\n\nSifreleme algoritmasi calistiriliyor..\n");
        encrypt(orginial_text, encrypted_text, text_size);
        printf("\nSIFRELENMIS mesaj:\n");
        for (i = 0; i <= text_size; i++) {
            encrypted_text_s[i] = (unsigned int)encrypted_text[i];
        }
        encrypted_text_s[i] = '\0';
        puts(encrypted_text_s);

        printf("\nSifrelenmis mesaj (ASCII kodu):\n");
        for (i = 0; i <= text_size; i++) {
            printf("%4u", (unsigned int)encrypted_text[i]);
        }

        // Şifre çözme işlemi
        printf("\n\nSimdi, sifrelenmis mesaj uzerinde sifreyi cozme algoritmasi calistiriliyor..\n");
        decrypt(encrypted_text, decrypted_text, text_size);
        printf("\nSIFRESI COZULMUS mesaj:\n");
        decrypted_text[text_size + 1] = '\0';
        puts(decrypted_text);
        printf("\nSifresi cozulmus mesaj (ASCII kodu):\n");
        for (i = 0; i <= text_size; i++) {
            printf("%4u", decrypted_text[i]);
        }

        printf("\n\n");

        // ASIL mesaj ile ŞİFRESİ ÇÖZÜLMÜŞ mesaj karşılaştır
        // Eğer her bir karakter karşılıklı olarak AYNI ise başarılı
        // DEĞİL ise BAŞARISIZ demektir
        int errcnt = strcmp_(orginial_text, decrypted_text);
        float student_score; // öğrencinin notu
        if (!errcnt) {
            printf("\t\tTEBRIKLER!\n"
                "ASIL metin ile SIFRESI COZULMUS metin AYNIDIR.\n"
                "SIFRELEME ve SIFRE COZME algoritmaniz DUZGUN calismaktadir.\n");
            student_score = 100;
        }
        else {
            float errprcnt = 100 * errcnt / text_size;
            printf("\t\tHATALI SONUC!\n"
                "ASIL metin ile SIFRESI COZULMUS metin ayni DEGILDIR.\n"
                "SIFRELEME ve SIFRE COZME algortimaniz YANLIS calismaktadir.\n"
                "Toplam %d karakterden %d adedi farkli cikmistir.\n"
                "Hata yuzdesi: %% %f\n", text_size, errcnt, errprcnt);
            if (errprcnt >= 10) {
                student_score = 0;
            }
            else {
                student_score = (10 - errprcnt) * 10;
            }
        }

        puts("");

        printf("\tOGRENCI PUANI: %f\n", student_score);
    }


    // FONKSİYONLARINIZI AŞAĞIDAKİ KÜÇÜK KOD PARÇALARIYLA TEST EDEBİLİRSİNİZ
    // Emin olduktan sonra karakter dizinlerini şifrelemeye geçin.

    // cosinus ve ters cosinus fonksiyonlarının testi için
    /*
    // aşağıda angle_d değişkenine [0, 180] derece arasında farklı değerler verip test edin
    double angle_d = 10; // derece cinsinden açı
    double angle_r = angle_d * 2 * M_PI / 360;
    double cosval = cos_(angle_r);
    printf("angle: %.4lf => cos(angle): %.4lf\n", angle_d, cosval);
    printf("val=cos(angle): %.4lf => angle in degrees=arccos(val): %.4lf", cosval, (360 / (2 * M_PI)) * arccos_(cosval));
    */

    // eksponensiyel ve doğal logaritma (log) fonksiyonlarının testi için
    /*
    // aşağıda x'e farklı değerler verip test edin (en fazla 10)
    double x = 10;  // ara sınav için yazılan fonksiyon en fazla 10 değeri alır, sınav ile uyumlu
    double expval = exp_(x);
    printf("x: %lf => exp(x): %lf\n", x, expval);
    printf("val=exp(x): %lf => x=log(val): %lf", expval, log_(expval));
    */

    // kare kök alma fonksiyonunun testi için
    /*
    // aşağıda x'e farklı değerler verip test edin
    double x = 0.1;
    double sqrval = x * x;
    printf("x: %lf => x^2: %lf\n", x, x * x);
    printf("val=x^2: %lf => x=sqrt(x^2): %lf", sqrval, sqrt_(sqrval));
    */

    // dizi elemanlarının yerini değiştirme fonksiyonunun testi için
    /*
    // aşağıdaki 4 farklı deneme metni ile deneyiniz (birini denerken diğerlerini yorum yapıp kapatınız)
    char test_text[SIZE] = "KalemKitapTahtaDolap";
    // char test_text[SIZE] = "KalemKitap1TahtaDolap";
    // char test_text[SIZE] = "Kalem1KitapTahta2Dolap";
    // char test_text[SIZE] = "Kalem1Kitap3Tahta2Dolap";
    size_t test_text_size = 0; // deneme metnindeki karakter sayısı
    // asıl deneme metninin bir kopyasını oluşturalım
    char test_text_cp[SIZE];
    while ((test_text_cp[test_text_size] = test_text[test_text_size]) != '\0') {
        // NULL karaktere kadar okur,
        // dizideki toplam karakter sayısını 'test_text_size' içinde tutar (dizi boyutu)
        // ve kopyalama işlemi yapar
        test_text_size++;
    }
    // önce deneme metninin ekrana yazdıralım
    printf("\nshuffleArray fonksiyonunun testi:\n"
        "Deneme metni asagidadir:\n\n");
    puts(test_text);
    // ardından bunun elemanlarının yerlerini değiştirelim
    shuffleArray(test_text_cp, test_text_size, 2); // karakter dizisi ile çalıştığımız için 3. argüman 2 olmalı
    // ve ekrana elemanlarının yerleri değiştirilmiş metni yazalım
    printf("\nDeneme metni shuffleArray fonksiyonundan gecirildikten sonra:\n");
    puts(test_text_cp);
    // aynı fonksiyondan bir daha geçirirsek eski haline dönmeli
    // eğer ilk metin ile aynısını ekranda okuyorsanız başarılı demektir
    // değil ise kodunuzu gözden geçiriniz
    shuffleArray(test_text_cp, test_text_size, 2);
    printf("\nDeneme metni TEKRAR shuffleArray fonksiyonundan gecirildikten sonra:\n");
    puts(test_text_cp);
    if (!strcmp(test_text, test_text_cp)) {
        printf("\n\t\tTEBRIKLER!\n"
            "shuffleArray fonksiyonunuz DUZGUN calismaktadir.\n");
    }
    else {
        printf("\n\t\HATALI SONUC!\n"
            "shuffleArray fonksiyonunuz YANLIS calismaktadir.\n");
    }
    */

}


// FONKSİYON TANIMLARI

// Cosinus fonksiyonunun tanımı:
// Cosinus fonksiyonunu, Taylor açılımından istifade ederek yaklaşık olarak ifade eder.
// Açılımdaki kesirli ifadelerin sayısı çözünrlüğü verir ve fonksiyon gövdesinde 'resolution' adlı
// bir tam sayı değişkeni ile ifade edilir. Bu sayıya, fonksiyonun istenilene yakın, kabul edilebilir
// bir değer döndürmesini sağlayacak kadar büyük bir değer verilmelidir.
// @input: açı değeri (radyan cinsinden) ondalıklı sayı
// @return: cosinus değeri [-1, +1] aralığında ondalıklı sayı
double cos_(double angle) {

    int resolution = 15; // Fonksiyonun BAŞARIMI üzerine DOĞRUDAN ETKİSİ vardır! YÜKSEK değer girin
    int counter_res = 1; // çözünürlük sayacı
    int n = 1; // cosinus hesaplamasında paydada kullanılan ve iki katının faktöriyeli alınan sayı
    double cos_computed = 1; // hesaplanan cosinus değeri

    // cosinüsü hesapla
    while (counter_res++ <= resolution) {
        // factoriel aşağıda hesaplanır
        int val = 0;
        // unsigned long long int fact = 1;
        double fact = 1;
        while (val <= 2 * n - 1) {
            // printf("\nval: %d", val);
            if (2 * n - val > 0)
                fact *= (2 * n - val);
            val++;
        }
        // printf("fact: %llu\n", fact);
        // factoriel hesabı tamamlandı

        double nom = 1; // cosinus hesaplamasında payda kullanılan 
                        // terim: açının kuvvetleri
        int counter_nom = 1; // pay için sayaç
        while (counter_nom++ <= 2 * n) {
            nom *= angle;
            // printf("num: %lf, angle: %lf, n: %d\n", num, angle, n);
        }
        // printf("num / fact: %.30lf, num: %lf, fact: %llu\n", (double) num / fact, num, fact);

        // şimdi çöznürlük döngüsündeki ilgili adımdan hesaplanmış cosinus
        // değerine katkıyı hesaplayalım
        cos_computed += pow(-1, n) * nom / fact;

        n++;
    }
    return cos_computed; // hesaplanmış cosinus değeri döndürülür
}

// Ters cosinus fonksiyonunun tanımı:
// Ters cosinus fonksiyonunu, Taylor açılımından istifade ederek yaklaşık olarak ifade eder.
// @input: cosinus değeri [-1, +1] aralığında ondalıklı sayı
// @return: açı değeri (radyan cinsinden)
double arccos_(double value) {

    int resolution = 60; // Fonksiyonun BAŞARIMI üzerine DOĞRUDAN ETKİSİ vardır! YÜKSEK değer girin
    int counter_res = 1; // çözünürlük sayacı
    int n = 0; // ters cosinus hesaplamasında kullanılan indis
    double arccos_computed = M_PI_2; // hesaplanan cosinus değeri - M_PI_2 = pi/2

    // ters cosinüsü hesapla
    while (counter_res++ <= resolution + 1) {
        // şimdi kombinasyon hesabı iki aşamada yapılacaktır
        // unsigned long long int comb; 
        double comb; // hesaplanan kombinasyon değerini tutar
        // bunun için her aşamada bir faktöriyel hesabı yapılacaktır
        // kombinasyon hesabı aşama 1 (2*n faktöriyelin hesabı)
        int val = 0;
        // unsigned long long int fact = 1;
        double fact = 1;
        while (val <= 2 * n - 1) {
            // printf("\nn: %d, val: %d, 2 * n - 1: %d", n, val, 2 * n - 1);
            if (2 * n - val > 0)
                fact *= (2 * n - val);
            val++;
        }
        comb = fact; // kombinasyon hesabının ilk aşaması tamamlandı
        // kombinasyon hesabı aşama 2 (n faktöriyelin hesabı)
        val = 0;
        fact = 1;
        while (val <= n - 1) {
            // printf("\nval: %d", val);
            if (n - val > 0)
                fact *= (n - val);
            val++;
        }
        comb /= fact; // kombinasyon hesabının ikinci ve son aşaması tamamlandı
        comb /= fact;

        // printf("komb: %llu\n", comb);
        // printf("komb: %lf\n", comb);

        double nom = 1; // ters cosinus hesaplamasında payda kullanılan 
                        // terimlerden diğeri: cosinus değerinin kuvvetleri
        int counter_nom = 1; // pay için sayaç
        while (counter_nom++ <= 2 * n + 1) {
            nom *= value;
            //  printf("num: %lf, value: %lf, n: %d\n", num, value, n);
        }
        // şimdi payda kullanılan tüm terimleri gözeterek pay değişkenini hesaplayalım
        nom *= comb;

        // şimdi paydadaki terimi hesaplayalım
        double den = pow(4, n) * (2 * n + 1);
        // printf("num / den: %.30lf, num: %lf, den: %llu\n", (double) num / den, num, den);

        // şimdi çöznürlük döngüsündeki ilgili adımdan hesaplanmış ters cosinus 
        // değerine (açı) katkıyı hesaplayalım
        arccos_computed -= nom / den;
        // printf("\n---n: %d, katki: %.10lf\n", n, arccos_computed);

        n++;
    }
    return arccos_computed; // hesaplanmış ters cosinus değeri döndürülür
}

// Eksponensyel fonksiyonunun tanımı:
// Eksponensiyel fonksiyonunu, Taylor açılımından istifade ederek yaklaşık olarak ifade eder.
// Açılımdaki kesirli ifadelerin sayısı çözünrlüğü verir ve fonksiyon gövdesinde 'resolution' adlı
// bir tam sayı değişkeni ile ifade edilir. Bu sayıya, fonksiyonun istenilene yakın, kabul edilebilir
// bir değer döndürmesini sağlayacak kadar büyük bir değer verilmelidir.
// @input - x: eksponensiyel fonksiyonu bulunacak ondalıklı sayı
// @return: eksponensiyel fonksiyonu değeri - ondalıklı sayı
double exp_(double x) {

    int resolution = 30; // Fonksiyonun BAŞARIMI üzerine DOĞRUDAN ETKİSİ vardır! YÜKSEK değer girin
    int counter_res = 1; // çözünürlük sayacı
    int n = 1; // eksponensiyel hesaplamasında kullanılan indis
    double e_computed = 1, e_pow_x_computed = 1; // hesaplanan Euler (e) sayısı ve
                                                 // eksponensiyel fonksiyon değeri

    while (counter_res++ <= resolution) {
        // factoriel hesabı yapılır
        int val = 0;
        double fact = 1;
        while (val <= n - 1) {
            // printf("\nval: %d", val);
            if (n - val > 0)
                fact *= (n - val);
            val++;
        }
        // factoriel hesabı tamamlanır - faktöriyel, paydada kullanılır
        // printf("fact: %lf\n", fact);

        e_computed += (1 / fact); // e sayısına çözünürlük döngüsünde ilgili adımından katkı

        double nom = 1; // eksponensiyel hesaplamasında payda kullanılan 
                        // terim: x'in kuvvetleri
        int counter_nom = 1; // pay için sayaç
        while (counter_nom++ <= n) {
            // printf("counter_num: %d, n: %d\n", counter_num, n);
            nom *= x;
            // printf("num: %lf, x: %lf, n: %d\n", num, x, n);
        }
        // printf("num / fact: %lf, num: %lf, fact: %lf\n", num / fact, num, fact);

        e_pow_x_computed += nom / fact; // eksponensiyel fonksiyonuna çözünürlük döngüsünde
                                        // ilgili adımından katkı
        n++;
    }
    // printf("\ncomputed e: %lf", e_computed);
    // printf("\ncomputed e(x): %lf", e_pow_x_computed);

    return e_pow_x_computed;
}

// Doğal logaritma fonksiyonunun tanımı:
// Doğal logaritma fonksiyonunu, 'xxx' açılımından istifade ederek yaklaşık olarak ifade eder.
// Girdisi 10'a kadar olduğunda güzel çalışıyor (10'dan büyük girdiler için çözünürlüğün ayarlanması
// gerekli - ileride daha güzel bir yaklaşıklamaya bakılabilir).
// Ara sınavda bu fonksiyona gelebilecek en büyük girdi 10 değerinde olabilir. Sıkıntısız çalışmaktadır.
// Açılımdaki kesirli ifadelerin sayısı çözünrlüğü verir ve fonksiyon gövdesinde 'resolution' adlı
// bir tam sayı değişkeni ile ifade edilir. Bu sayıya, fonksiyonun istenilene yakın, kabul edilebilir
// bir değer döndürmesini sağlayacak kadar büyük bir değer verilmelidir.
// @input - value: doğal logaritması alınacak ondalıklı sayı
// @return: doğal logaritma fonksiyonu değeri - ondalıklı sayı
double log_(double value) {

    int resolution = 20000; // Fonksiyonun BAŞARIMI üzerine DOĞRUDAN ETKİSİ vardır! YÜKSEK değer girin
    int counter_res; // çözünürlük sayacı 

    double nom; // pay
    double den; // payda
    double calc = (value - 1) / (value + 1); // paydaki terim
    //printf("calc: %lf\n", calc);
    double log_computed = 0; // hesaplanan logaritma değeri

    // terminating value of the loop
    // can be increased to improve the precision
    for (counter_res = 1; counter_res <= resolution; counter_res++) {
        den = 2 * counter_res - 1;  // payda hesaplanır
        nom = pow(calc, den);       // pay hesaplanır
        // printf("n: %d, nom / den: %lf, nom: %lf, den: %lf\n", counter_res, nom / den, nom, den);
        log_computed += nom / den;  // hesaplanan logaritmaya çözünürülük döngüsünün ilgili
                                    // adımından katkı
    }
    log_computed *= 2;      // logaritma hesabı tamamlanır

    return log_computed;
}

// Kare kök alma fonksiyonunun tanımı:
// Kare kök alma fonksiyonunu, 'xxx' açılımından istifade ederek yaklaşık olarak ifade eder.
// Açılım adım adım ilerleyerek gerçek kare köke yakınsar. Adımların sayısı çözünrlüğü verir ve fonksiyon gövdesinde 'resolution' adlı
// bir tam sayı değişkeni ile ifade edilir. Bu sayıya, fonksiyonun istenilene yakın, kabul edilebilir
// bir değer döndürmesini sağlayacak kadar büyük bir değer verilmelidir.
// @input - x: akre kökü bulunacak ondalıklı sayı
// @return: kare kök değeri - ondalıklı sayı
double sqrt_(double x) {

    double guess = 0;    // x'e en yakın tam kera kökü tahmini
    double mrgn = 0.001; // algoritma adımlarında 0'a bölme işleminden kurutlmak için

    int resolution = 20; // Fonksiyonun BAŞARIMI üzerine DOĞRUDAN ETKİSİ vardır! YÜKSEK değer girin
    int counter_res = 1; // çözünürlük sayacı 

    // x'e en yakın tam kare kökü bulalım (binary search - ikili arama)
    int start = 0, end = (int)x, mid; // arama için başlangıç, bitiş ve orta elemanlar
    while (start <= end) {
        // orta elemanı bulalım
        mid = (start + end) / 2;
        // eğer sayı tam kare ise bitir
        if (mid * mid == x) {
            guess = mid;
            break;
        }
        // eğer tam kare, orta elemandan büyük ise başlangıç elemanını arttır
        if (mid * mid < x) {
            // önce, başlangıç elemanı tahminimiz eklenmeli
            guess = start;
            // sonra, başlangıç elemanını güncelle
            start = mid + 1;
        }
        // eğer tam kare, orta elemandan küçük ise bitiş elemanını azalt
        else {
            end = mid - 1;
        }
    }
    // x' en yakın tam kare kökün tam sayı kısmı bulundu

    // printf("guess: %d\n", guess);

    // şimdi adım adım ilerleyerek x'in gerçek kare köküne yakınsayalım
    double d;              // x ile en yakın tam kare tahmini arasındaki fark
    double P;              // ara değişken
    double A;              // ara değişken 
    double sqrt_comuted = guess;   // hesaplanmış kare kök
    while (counter_res++ <= resolution) {
        d = x - sqrt_comuted * sqrt_comuted;    // x ile en yakın tam kare tahmini arasındaki fark
        P = d / (2 * sqrt_comuted + mrgn); // ara değişken
        A = sqrt_comuted + P;       // ara değişken 
        sqrt_comuted = A - ((P * P) / (2 * A));   // hesaplanmış kare kök her adımda güncellenir
    }

    return sqrt_comuted; // // hesaplanmış kare kök döndürülür
}

// Dizi elemanlarının yerini değiştirme fonksiyonunun tanımı:
// Şifrelenecek/şifresi çözülecek metindeki karakterlerin yerlerini değiştirir.
// Herhangi bir türde eleman tutan dizi üzerinde işlem yapılabilmesi için değişken türü olarak
// void seçilmiştir. Fonksiyon gövdesinde, hangi türde değişkenle işlem yapılacaksa o türe 
// dönüşümü yapılır (type casting). 
// @input - data_array: elemanlarının yeri değiştirilecek dizinin başlangıç adresi
// @input - array_size: bu dizinin boyutu
// @input - castingtype: 1 ise float tipine, 2 ise char tipine dönüşüm yapılır
// @return - elemanarının yeri değiştirilmiş dizinin başlangıç adresi
void shuffleArray_l(void* data_array, size_t array_size, int castingtype) {

    // karakter dizimizin boyutu şu 4 olasılıktan birini sağlar (d: boyut, n ise bir tam sayı olsun)
    // karakter dizimizin boyutu şu 4 olasılıktan birini sağlar (d: boyut, n ise bir tam sayı olsun)
    // 1) d = 2 * (2 * n)           = 4 * n
    // 2) d = 2 * (2 * n)       + 1 = 4 * n + 1 
    // 3) d = 2 * (2 * n + 1)       = 4 * n + 2
    // 4) d = 2 * (2 * n + 1)   + 1 = 4 * n + 3
    // dizi boyutunun bunlardan hangisini sağladığını belirlemeliyiz ve sonrasında
    // diziyi dört alt dizi olarak ele almalıyız
    int n = array_size / 4;
    int quartxstart[4] = { 0 };  // dizinin x numaralı dörtlüğünün başlangıç indisi, x = 1, 2, 3, 4
    int quartxend[4];            // dizinin x numaralı dörtlüğünün bitiş indisi, x = 1, 2, 3, 4
    switch (array_size % 4) {
    case 0:     // d = 4 * n durumu
        quartxstart[1] = n; quartxstart[2] = 2 * n; quartxstart[3] = 3 * n;
        quartxend[0] = n - 1;  quartxend[1] = 2 * n - 1; quartxend[2] = 3 * n - 1; quartxend[3] = 4 * n - 1;
        break;
    case 1:     // d = 4 * n + 1 durumu
        quartxstart[1] = n; quartxstart[2] = 2 * n + 1; quartxstart[3] = 3 * n + 1;
        quartxend[0] = n - 1;  quartxend[1] = 2 * n - 1; quartxend[2] = 3 * n; quartxend[3] = 4 * n;
        break;
    case 2:     // d = 4 * n + 2 durumu
        quartxstart[1] = n + 1; quartxstart[2] = 2 * n + 1; quartxstart[3] = 3 * n + 2;
        quartxend[0] = n - 1;  quartxend[1] = 2 * n; quartxend[2] = 3 * n; quartxend[3] = 4 * n + 1;
        break;
    case 3:     // d = 4 * n + 3 durumu
        quartxstart[1] = n + 1; quartxstart[2] = 2 * n + 2; quartxstart[3] = 3 * n + 3;
        quartxend[0] = n - 1;  quartxend[1] = 2 * n; quartxend[2] = 3 * n + 1; quartxend[3] = 4 * n + 2;
        break;
    }
    // şimdi 4 alt dizide karşılıklı gelenler arasında eleman değiştirme yapalım
    // ilk yarıdaki iki alt dizi (1. ve 2. dörtlük) ile 
    // ikinci yarıdaki iki alt dizi (3. ve 4. dörtlük) arasında yer değiştirme yapılır
    for (int i = 0; i <= 1; i++) {
        // arasında eleman değişimi gerçekleştirilecek iki adet alt dizinin toplam 4 adet alt dizi
        // içindeki indisleri
        int firstarrayind = 2 * i, secondarrayind = 2 * i + 1;
        // ilgili iki alt dizi arasında eleman değiştirme başlar
        for (int j = 0; j < n; j++) {
            if (castingtype == 1) {
                // eleman yerleştirmeden hemen önce gerekli tür dönüşümü yapılır 
                // aşağıda tanımlı 'myarray', artık elemanlarının yerini değiştirdiğimiz diziyi (data_array) göstermektedir
                // onun üzerinde yaptığımız değişiklikler doğrudan 'data_array' üzerinde yapılmış olur.
                // castingtype = 1 => float
                float* myarray = (float*)data_array;
                float temp = myarray[quartxstart[firstarrayind] + j];
                myarray[quartxstart[firstarrayind] + j] = myarray[quartxend[secondarrayind] - j];
                myarray[quartxend[secondarrayind] - j] = temp;
            }
            else {
                // castingtype = 2 = > char
                char* myarray = (char*)data_array;
                char temp = myarray[quartxstart[firstarrayind] + j];
                myarray[quartxstart[firstarrayind] + j] = myarray[quartxend[secondarrayind] - j];
                myarray[quartxend[secondarrayind] - j] = temp;
            }

        }
    }
}

// Dizi elemanlarının yerini değiştirme fonksiyonunun tanımı:
// Şifrelenecek/şifresi çözülecek metindeki karakterlerin yerlerini değiştirir.
// Herhangi bir türde eleman tutan dizi üzerinde işlem yapılabilmesi için değişken türü olarak
// void seçilmiştir. Fonksiyon gövdesinde, hangi türde değişkenle işlem yapılacaksa o türe 
// dönüşümü yapılır (type casting). 
// @input - data_array: elemanlarının yeri değiştirilecek dizinin başlangıç adresi
// @input - array_size: bu dizinin boyutu
// @input - castingtype: 1 ise float tipine, 2 ise char tipine dönüşüm yapılır
// @return - elemanarının yeri değiştirilmiş dizinin başlangıç adresi
void shuffleArray_s(void* data_array, size_t array_size, int castingtype) {

    // karakter dizimizin boyutu şu 4 olasılıktan birini sağlar (d: boyut, n ise bir tam sayı olsun)
    // karakter dizimizin boyutu şu 4 olasılıktan birini sağlar (d: boyut, n ise bir tam sayı olsun)
    // 1) d = 2 * (2 * n)           = 4 * n
    // 2) d = 2 * (2 * n)       + 1 = 4 * n + 1 
    // 3) d = 2 * (2 * n + 1)       = 4 * n + 2
    // 4) d = 2 * (2 * n + 1)   + 1 = 4 * n + 3
    // dizi boyutunun bunlardan hangisini sağladığını belirlemeliyiz ve sonrasında
    // diziyi dört alt dizi olarak ele almalıyız
    int n = array_size / 4;
    int quartxstart[4] = { 0 };  // dizinin x numaralı dörtlüğünün başlangıç indisi, x = 1, 2, 3, 4
    int quartxend[4];            // dizinin x numaralı dörtlüğünün bitiş indisi, x = 1, 2, 3, 4
    switch (array_size % 4) {
    case 0:     // d = 4 * n durumu
        quartxstart[1] = n; quartxstart[2] = 2 * n; quartxstart[3] = 3 * n;
        quartxend[0] = n - 1;  quartxend[1] = 2 * n - 1; quartxend[2] = 3 * n - 1; quartxend[3] = 4 * n - 1;
        break;
    case 1:     // d = 4 * n + 1 durumu
        quartxstart[1] = n; quartxstart[2] = 2 * n + 1; quartxstart[3] = 3 * n + 1;
        quartxend[0] = n - 1;  quartxend[1] = 2 * n - 1; quartxend[2] = 3 * n; quartxend[3] = 4 * n;
        break;
    case 2:     // d = 4 * n + 2 durumu
        quartxstart[1] = n + 1; quartxstart[2] = 2 * n + 1; quartxstart[3] = 3 * n + 2;
        quartxend[0] = n - 1;  quartxend[1] = 2 * n; quartxend[2] = 3 * n; quartxend[3] = 4 * n + 1;
        break;
    case 3:     // d = 4 * n + 3 durumu
        quartxstart[1] = n + 1; quartxstart[2] = 2 * n + 2; quartxstart[3] = 3 * n + 3;
        quartxend[0] = n - 1;  quartxend[1] = 2 * n; quartxend[2] = 3 * n + 1; quartxend[3] = 4 * n + 2;
        break;
    }
    // şimdi 4 alt dizide karşılıklı gelenler arasında eleman değiştirme yapalım
    // ilk yarıdaki iki alt dizi (1. ve 2. dörtlük) ile 
    // ikinci yarıdaki iki alt dizi (3. ve 4. dörtlük) arasında yer değiştirme yapılır
    for (int i = 0; i <= 1; i++) {
        // arasında eleman değişimi gerçekleştirilecek iki adet alt dizinin toplam 4 adet alt dizi
        // içindeki indisleri
        int firstarrayind = 2 * i, secondarrayind = 2 * i + 1;
        // ilgili iki alt dizi arasında eleman değiştirme başlar
        for (int j = 0; j < n; j++) {
            if (castingtype == 1) {
                // eleman yerleştirmeden hemen önce gerekli tür dönüşümü yapılır 
                // aşağıda tanımlı 'myarray', artık elemanlarının yerini değiştirdiğimiz diziyi (data_array) göstermektedir
                // onun üzerinde yaptığımız değişiklikler doğrudan 'data_array' üzerinde yapılmış olur.
                // castingtype = 1 => float
                float* myarray = (float*)data_array;
                float temp = myarray[quartxstart[firstarrayind] + j];
                myarray[quartxstart[firstarrayind] + j] = myarray[quartxend[secondarrayind] - j];
                myarray[quartxend[secondarrayind] - j] = temp;
            }
            else {
                // castingtype = 2 = > char
                char* myarray = (char*)data_array;
                char temp = myarray[quartxstart[firstarrayind] + j];
                myarray[quartxstart[firstarrayind] + j] = myarray[quartxend[secondarrayind] - j];
                myarray[quartxend[secondarrayind] - j] = temp;
            }

        }
    }
}

// Şifreleme fonksiyonunun tanımı:
// Şifrelenecek metindeki karakterlerin ASCII kod karşılıklarını cosinus fonksiyonundan geçirir.
// @input - original_text: şifrelenecek metin
// @input - encrypted_text: şifrelenmiş metin
// @input - text_size: metindeki karakter sayısı 
void encrypt(const char* original_text, float* encrypted_text, size_t text_size) {

    // önce, her bir karaktere şifreleme işlemi uygulayalım
    for (size_t i = 0; i <= text_size; i++) {
        float temp = exp_(1 + cos_((2 * M_PI / 360) * original_text[i]));
        encrypted_text[i] = 3 * (temp * temp);
        // printf("encrypted_text[%d]: %.10lf\n", i, encrypted_text[i]);
    }
    // sonra, karakterlerin yerlerini değiştirelim
    shuffleArray_l(encrypted_text, text_size, 1);
    // şifreleme TAMAM!
}

// Şifre çözme fonksiyonunun tanımı:
// Şifrelenmiş metindeki karakterlerin ASCII kod karşılıklarını ters cosinus fonksiyonundan geçirir.
// @input - encrypted_text: şifrelenmiş metin
// @input - decrypted_text: şifresi çözülmüş metin
// @input - text_size: metindeki karakter sayısı 
void decrypt(const float* encrypted_text, char* decrypted_text, size_t text_size) {

    // önce, her bir karaktere şifre çözme işlemi uygulayalım
    for (size_t i = 0; i <= text_size; i++) {
        // Bu değiken, şifresi çözülmüş değeri ondalıklı sayı olarak tutar. 
        float temp = (360 / (2 * M_PI)) * arccos_(log_(sqrt_((double)encrypted_text[i] / 3)) - 1);
        // Ancak, ASCCII kodlar tam sayıdır.
        // Bazen fonskiyonlar örneğin 43 döndürmesi gerekirken 42.9999 döndürmektedir. 
        // Ya da 43.0001 de döndürebilmektedir.
        // Böyle olanları en yakın tam sayıya yuvarlamamız gerekli.
        unsigned int intpart = (unsigned int)temp;
        if (temp - intpart <= 0.5) {
            decrypted_text[i] = intpart;
        }
        else {
            decrypted_text[i] = intpart + 1;
        }
        // printf("decrypted_text[%d] as floating point number: %.10lf\n", i, temp);
    }
    // sonra, karakterlerin yerlerini değiştirelim ve eski haline getirelim
    shuffleArray_s(decrypted_text, text_size, 2);
    // şifre çözme TAMAM!
}

// Karakter dizilerini karşılaştırma fonksiyonunun tanımı:
// İki adet karakter dizisini karşılaştırır. Her iki dizi de aynı boyutta olmalıdır.
// İlk dizide NULL karakterlerle karşılaşasıya kadar karakterleri okur.
// @input - str1: karşılaştırılacak karakter dizisi
// @input - str2: karşılaştırılacak diğer karakter dizisi
// @return - text_size: karşılaştırma soncunda farklı çıkan karakter sayısı
//                      (eğer sonuç sıfır ise iki dizi bir biriyle aynıdır)
int strcmp_(const char* str1, const char* str2) {

    int i = 0, err = 0;
    while (str1[i] != '\0') {
        if (str1[i] != str2[i])
            err++;
        i++;
    }
    return err; // aynı olmayan karakter sayısı
}


/*
// T�rk�e karakterleri �ngilizce karakterlerle de�i�tirme tan�m�:
// Bir metin dosyas�ndaki T�rk�e karakterleri �ngilizce karakterlerle de�i�tirir.
// @input - text_file: karakterleri de�i�tirilecek metin dosyas�n�n ismi
// @return: ba�ar� durumunda, de�i�itirilen karakter say�s� (dosya var ve a��l�yor), di�er durumda -1
int replaceTRwEN(const char* file_name) {
    
    // lower = "abcdefghijklmnopqrstuvwxyz�����"
    // upper = "ABCDEFGH�JKLMNOPQRSTUVWXYZ����I"
    const wchar_t lower[] = L"abcdefghijklmnopqrstuvwxyz\u00E7\u00F6\u00FC\u011F\u0131";
    const wchar_t upper[] = L"ABCDEFGH\u0130JKLMNOPQRSTUVWXYZ\u00C7\u00D6\u00DC\u011EI";

    const char* cwd = _getcwd();
    char abspathtofile[100];
    sprintf(abspathtofile, "%s\\Midterm\\%s", cwd, file_name);
    // printf("Absolute path to file: %s\n", abspathtofile);

    FILE* text_file_p = fopen(abspathtofile, "r+"); // open the file with read and write privilidges
    if (!text_file_p) {
        printf("ERROR! File %s cannot be opened.\n", file_name);
        return -1;
    }
    else {
        fpos_t pos; // position index for reading from and writing onto the file
        char c;
        int cnt = 0; // de�i�tirilen karakterlerin say�s�
        while ((c = fgetc(text_file_p)) != EOF) { // read one character at a time from the file
                                                  // until End-Of-File is reaxhed
            printf("%c", c);
            // fgetpos(text_file_p, &pos); // get the current position index 
            // printf("pos: %d\n", pos);
            switch (c) {
                case 'ç': // �  \u00E7
                    fputc('c', text_file_p);
                    cnt++;
                    break;
                case \u00C7: // �
                    fputc('C', text_file_p);
                    cnt++;
                    break;
                case \u011F: // �
                    fputc('g', text_file_p);
                    cnt++;
                    printf("buradayiz\n");
                    break;
                case \u00DC: // �
                    fputc('G', text_file_p);
                    cnt++;
                    break;
                case \u0131: // �
                    fputc('i', text_file_p);
                    cnt++;
                    break;
                case '�': // �
                    fputc('I', text_file_p);
                    cnt++;
                    break;
                case \u00F6: // �
                    fputc('o', text_file_p);
                    cnt++;
                    break;
                case \u00D6: // �
                    fputc('O', text_file_p);
                    cnt++;
                    break;
                case '�': // �
                    fputc('s', text_file_p);
                    cnt++;
                    break;
                case '�': // �
                    fputc('S', text_file_p);
                    cnt++;
                    break;
                case \u00FC: // �
                    fputc('u', text_file_p);
                    cnt++;
                    break;
                case '�': // �
                    fputc('U', text_file_p);
                    cnt++;
                    break;
                }
        }
        fclose(text_file_p); // all Turkish caharcters are replaced with English ones, close the file
        return cnt;
    }
}
*/