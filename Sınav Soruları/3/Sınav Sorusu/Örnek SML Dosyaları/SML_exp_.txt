22099	% hafızada 99 numaralı adrese hemen bir değer yükle (bir sonraki adresteki değer)
00400	% 400 değerini yükle (99 numaralı adresi resolution değişkenini tutmak için kullanıyoruz)
22098	% hafızada 98 numaralı adrese hemen bir değer yükle (bir sonraki adresteki değer)
00001	% 1 değerini yükle (98 numaralı adresi counter_res değişkenini tutmak için kullanıyoruz)
22097	% hafızada 97 numaralı adrese hemen bir değer yükle (bir sonraki adresteki değer)
00000	% 0 değerini yükle (97 numaralı adresi n değişkenini tutmak için kullanıyoruz)
22096	% hafızada 96 numaralı adrese hemen bir değer yükle (bir sonraki adresteki değer)
00001	% 1 değerini yükle (96 numaralı adresi term değişkenini tutmak için kullanıyoruz)
20096	% akümülatöre term değişkeninin değerini yükle
21095	% akümülatördeki değeri (term değişkenini değerini) 95 numaralı adrese yükle (95 numaralı adresi e_pow_x_computed değişkenini tutmak için kullanıyoruz)
20099	% resolution değişkeinini akümülatöre yükle - while döngüsünün başı (döngü çalışma koşulunun olduğu kısım)
31098	% akümülatörden counter_res değişkenini çıkar ve sonucu (resolution - counter_res) akümülatörde tut
41030	% sonuç negatif ise satır 30'a sıçra
20097	% sonuç negatif değil ise counter_res <= resolution koşulu sağlanıyor demektir ve while gövdesindeki işlemleri sırayla yap - x / (n + 1)'i hesaplamak için önce akümülatöre n'yi yükle
34000	% akümülatördeki değeri (n) 1 arttır ve sonucu akümülatörde tut
21094	% akümülatördeki değeri (n+1) 94 numaralı adrese yükle
20101	% akümülatöre x'i yükle - x, girdidir ve girdi yazmacı 1'de saklıdır
32094	% akümülatördeki değeri (x) 94 numaralı adresteki değere (n+1) böl ve sonucu akümülatörde sakla
33096	% akümülatördeki değeri (x/(n+1)) term değişkeni ile çarp ve sonucu akümülatörde tut
21096	% akümülatördeki değeri (term * x/(n+1)) term değişkenine yükle - böylece term değişkeninin özyinelemeli güncellemesi gerçekleştirilmiş olur
20095	% akümülatöre e_pow_x_computed deişkenini yükle
30096	% akümülatördeki değere (e_pow_x_computed) term değişkenini ekle ve sonucu akümülatörde tut
21095	% akümülatördeki değeri (e_pow_x_computed + term) e_pow_x_computed değişkenine yükle - böylece e_pow_x_computed değişkeninin güncellemesi de yapılmış olur
20097	% n değişkenini akümülatöre yükle
34000	% akümülatördeki değeri (n) 1 arttır ve sonucu akümülatörde tut
21097	% akümülatördeki değeri (n+1) n değişkenine ata - böylece n++ işlemi gerçekleştirilmiş oldu
20098	% counter_res değişkenini akümülatöre yükle
34000	% akümülatördeki değeri (counter_res) 1 arttır ve sonucu akümülatörde tut
21098	% akümülatördeki değeri (counter_res+1) counter_res değişkenine ata - böylece counter_res++ işlemi gerçekleştirilmiş oldu
40010	% 10 numaralı satıra sıçra (while döngüsünün başı yani döngünün çalışma koşulunun sağlanıp sağlanmadığının kontrol edildiği satır)
50095	% satır 30: while döngüsü bitmiştir, e_pow_x_computed değişkenini döndür (return e_pow_x_computed)
43000	% programı durdur