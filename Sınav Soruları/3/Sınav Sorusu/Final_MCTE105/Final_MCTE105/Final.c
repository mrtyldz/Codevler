/*************************************************************/
/*                                                           */
/*                  Written by                               */
/*                 Erdem Dilmen                              */
/*           Mechatronics Engineering                        */
/*              Pamukkale University                         */
/*                  4.1.2022                                 */
/*                                                           */
/*************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <io.h>

#define _USE_MATH_DEFINES   // pi say�s�n� kullanmak isterseniz bu gereklidir
                            // bu say�, M_PI sembolik sabiti ile g�sterilmektedir
#include <math.h>           // pi say�s�n�n kullan�labilmesi i�in dahil edilir

int SML_sim(const char* SML_file); // SML makine dilinde benzetimcisi (simulator) - fonksiyon prototipi

#define CODEMEMSIZE 100 // kod haf�zas�n�n i�erdi�i kelime (word) say�s� cinsinden boyutu

#define INREGCNT 2  // girdi yazmac� say�s� 2 olarak belirlenmi�tir
#define OUTREGCNT 1 // // ��kt� yazmac� say�s� 1 olarak belirlenmi�tir

#define REGSTARTADDRESS CODEMEMSIZE // yazma�lar�n ba�lang�� adresi kod haf�zas�n�n boyutuna e�ittir
                                    // kod haf�zas�n�n bitti�i konumdan yazma�lar haf�zada yerle�meye ba�lar

#define ACCADDRESS REGSTARTADDRESS // ak�m�lat�r�n adresi - ilk yazma� ak�m�lat�rd�r

#define INREGSTARTADDRESS (ACCADDRESS + 1) // girdi yazma�lar�n�n ba�lang�� adresi - hemen ak�m�lat�rden sonra ba�lar
#define INREGENDADDRESS (ACCADDRESS + INREGCNT) // girdi yazma�lar�n�n biti� adresi

#define OUTREGSTARTADDRESS (INREGENDADDRESS + 1) // ��kt� yazma�lar�n�n ba�lang�� adresi - hemen girdi yazma�lar�ndan sonra ba�lar
#define OUTREGENDADDRESS (INREGENDADDRESS + OUTREGCNT) // ��kt� yazma�lar�n�n biti� adresi


// a�a��daki yazma�lar SML dilinde yaz�lacak fonksiyonlar�n girdi ve ��kt�lar�n� tutar
double inreg[INREGCNT]; // girdi yazma�lar� (fonksiyon girdisinin de�erini tutar - input register) - haf�za konumu 101-102
double outreg[OUTREGCNT]; // ��kt� yazma�lar� (fonksiyonun d�nd�rece�i de�erini tutar - input register) - haf�za konumu 103


#define PATH_MAX 500    // �zerinde �al���lan klas�rdeki SML dosyas�n�n tam ad�n� tutacak (kals�r adresi �eklinde)
#define MAX_LENGTH 500  // SML dosyas�ndan her bir sat�r okunurken okunacak en fazla karakter say�s�

#define _DEBUG 0

// fonksiyon tan�m�: haf�zaya yaz�lm�� olan komutlar� g�sterir
// @in: memory - haf�zadaki komutlar� tutan dizi, size - bu dizinin boyutu
// @out: void
void displayMemory(double memory[], int size) {

    int i;

    for (i = 0; i < size; i++) {
        if (!(memory[i] - (long long int)memory[i])) // e�er haf�zada saklanan de�er tam say� t�r�nde ise
            printf(" %010lld ", (long long int)memory[i]);
        else // ondal�kl� say� t�r�nde ise
            printf(" %010.3lf ", memory[i]);

        if ((i + 1) % 10 == 0) {
            printf("\n");
        }
    }

    printf("\n");

}

// ANA FONKS�YON

void main() {
                    // 89.999   
    double angle_d = 89.999;    // derece cinsinden a��
    double sinval;              // bu a��n�n sinusu
    double asinval;             // bu sinus degerinin ters sinusu (a��n�n kendisini vermesi beklenir)
    const char* SML_file_sin_ = "SML_sin_.txt";  // sinus fonksiyonu i�in SML dili kodlar�n� i�eren dosya - SML_sin_.txt
    const char* SML_file_asin_ = "SML_asin_.txt";  // ters sinus fonksiyonu i�in SML dili kodlar�n� i�eren dosya - SML_asin_.txt

    double angle_r = angle_d * (2 * M_PI / 360);
    inreg[0] = angle_r; // girdi (SML dosyas� i�indeki fonksiyon olarak �al��an kodun girdisi - inreg i�inde tutulur)
    printf("\n\tSINUS HESAPLANIYOR\t\n");
    SML_sim(SML_file_sin_); // bilgisayar �al���yor - sinus hesab�
    sinval = outreg[0]; // ��kt� (SML dosyas� i�indeki kodun d�nd�rd��� de�er - outreg i�inde tutulur)

    inreg[0] = sinval;
    printf("\n\tTERS SINUS HESAPLANIYOR\t\n");
    SML_sim(SML_file_asin_); // bilgisayar tekrar �al���yor - ters sinus hesab�
    asinval = (360 / (2 * M_PI)) * outreg[0];

    printf("aci: %.10lf => sin_(aci): %.10lf\n", angle_d, sinval);      // * (360 / (2 * M_PI))
    printf("deger=sin_(aci): %.10lf => aci derece cinsinden=asin_(deger): %.10lf\n", sinval, asinval);

    double error = angle_d - asinval; // ters sinus sonucu ile a�� aras�ndaki hata - ondal�kl� say�
    double student_score; // ��renci puan�
    if (error < 0)
        error *= -1;    // hatan�n mutlak de�eri gerekli
    printf("\nHATA: %lf\n", error);

    if (error >= 0.1) // e�er hata 0.1'den b�y�k veya e�it bir de�ere sahipse ��renci 0 puan al�r
        student_score = 0;
    else if (error >= 0.05 && error < 0.1) // bu hata aral��� i�in ��renci paun� ��yle hesaplan�r
        student_score = (0.1 - error) * 2000;
    else            // hata 0.05'ten k���k ise ��renci 100 puan al�r
        student_score = 100;

    puts("");
    printf("\n\tOGRENCI PUANI: %lf\n", student_score);

}

// fonksiyon tan�m�: SML makine dilinde yaz�lan komutlar� dosyada okuyup �al��t�r�r
// @in: SML_file - SML komutlar�n�n okunaca�� dosyan�n ismi
// @out: 1 - ba�ar� durumunda, -1 - ba�ar�s�zl�k durumunda
int SML_sim(const char* SML_file) {

    double memory[CODEMEMSIZE] = { 0 }; // haf�zaya yaz�lacak makine dili komutlar�n� tutar
    long long int i = 0; // �al��t�r�lacak komutun bulundu�u adres de�erini tutan program sayac�
    long long int opcode = 0; // �al��t�r�lacak komut
    long long int address = 0; // komutun i�leyece�i arg�man yani adres
    double accumulator = 0; // aritmetik i�lemler i�in �zel yazma� (register) - haf�za konumu 100
    int counter = 0; // SML komutlar�n� ilgili dosyadan �ekip haf�zaya kaydederken kullan�lan saya�
    FILE* file; // SML.txt isimli ve SML komutlar�n� tutan dosyaya i�aret eden g�sterici (pointer)
    char cwd[PATH_MAX]; // �zerinde �al���lan klas�r adresini tutar (current working directory)
    char absolutefilename[PATH_MAX]; // tam yol ile beliritlen dosya ismi
    char codeline[MAX_LENGTH]; // SML dosyas�ndan her bir sat�r okunurken karakterler bu diziye kaydedilir
    char temp[50];
    int storei_flag = 0; // hemen de�er y�kleme (immediate operation) 
                       // opcode 22 �al��t�r�laca�� zaman i�lemciye bildirimde bulunan mant�ksal bayrak
                       // (logical flag - bayraklar program ak���n� kontrol etmeyi sa�lar)
    int reti_flag = 0; // hemen de�er d�nd�rme - opcode 51 �al��t�r�laca�� zaman i�lemciye bildirimde bulunan mant�ksal bayrak
    int addi_flag = 0; // hemen de�er ekleme - opcode 36 �al��t�r�laca�� zaman i�lemciye bildirimde bulunan mant�ksal bayrak
    int subi_flag = 0; // hemen de�er ��karma - opcode 37 �al��t�r�laca�� zaman i�lemciye bildirimde bulunan mant�ksal bayrak
    int divi_flag = 0; // hemen de�ere b�lme - opcode 38 �al��t�r�laca�� zaman i�lemciye bildirimde bulunan mant�ksal bayrak
    int muli_flag = 0; // hemen de�erle �arpma - opcode 39 �al��t�r�laca�� zaman i�lemciye bildirimde bulunan mant�ksal bayrak
    int skip_opcode = 0; // yukar�daki bayraklardan biri kalk�nca bu bayrak da kalkar

    if (getcwd(cwd, sizeof(cwd)) != NULL) { // SML dosya adresine ula�, 
                                            // SML.txt dosyas�, �zerinde �al���lan proje dosyas� (.vcxproj uzant�l�) ile ayn� klas�rde bulundurulmal�

        sprintf(absolutefilename, "%s\\%s", cwd, SML_file);
        // printf("\n%s\n", absolutefilename);
        if ((file = fopen(absolutefilename, "r")) == NULL) { // SML dosyas�n� a�
            printf("\nTemin edilen SML dosyasi acilmiyor\n");
            return -1;
        }
        else {
            // SML dosyas�n�n sonuna gelesiye kadar haf�zadan komut-arg�man �iftlerini oku ve haf�zaya yaz
            // bunu, dosyadan her seferde bir sat�r� okuyarak yap
            // sat�r okuma i�leminde komut-arg�man �iftiyle beraber '%' karakteriyle ba�layan yorumlar da okunur
            // ve codeline adl� karakter dizisne kaydedilir
            while (fgets(codeline, MAX_LENGTH, file)) {
                // printf("line %d: %s\n", i, codeline);
                char* pPtr = strchr(codeline, '%'); // SML dosyas�nda ilgili sat�rda '%' karakterinin bulundu�u konumu tespit et 
                                                    // ve pPtr isimli g�stericiyle bu konuma i�aret et
                char* sPtr = &codeline[0];
                int j = 0;
                while (sPtr != pPtr && *sPtr != '\0' && j < 50) { // codeline karakter dizisini '%' karakterine gelesiye kadar ba�tan sona tara ve 
                                       // s�rayla elemanlar�n� temp dizisine aktar
                    temp[j] = *sPtr;
                    // printf("*sPtr: %c, j: %d\n", *sPtr, j);
                    sPtr++;  j++;
                }
                temp[j] = '\0';

                memory[i] = atof(temp); // karakter dizisinden say�ya d�n��t�r�p haf�zaya yaz
                                       // opcode ve arg�man (adres) 4 basamakl� tam say�s� 
                                       // veya adres i�ine do�rudan y�klenecek de�er haf�zaya yaz�ld�
                                       // bu de�er ondal�kl� say� (float) tr�nde de olabilir ondan, haf�zay� temsil
                                       // eden diziyi (memory) cinsinden se�tik. E�er haf�zaya y�klenecek say� tams ay� ise
                                       // �rn., opcode-arg�man �ifti, bunu tam say� �eklinde ekrana yazd�r�r�z (displayMemory fonksiyonu i�inde)
                                       // Unutmay�n ki, opcode-arg�man �ifti tam say� oldu�u halde bir ondal�kl� say� dizisi i�inde tutlsa dahi
                                       // biz bunlarla halen gerekli i�lemleri yapabiliriz. ��nk�, asl�nda tam say�lar ondadl�kl� k�sm� 0 olan
                                       // olan ondal�kl� say�lar gibi yorumlanabilir (�rn., 45 tam say�s� asl�nda 45.00 olarak da youmlanabilir).

                // temp[j-1] = '\0';
                // printf("temp: %s\n", temp);

                i++;
            }

            fclose(file); // SML dosyas�n� kapat
            i = 0;

            printf("\n\n****SML dosyasi hafizaya yuklendikten sonra hafizanin durumu****\n\n");
            displayMemory(memory, CODEMEMSIZE); // haf�zaya yaz�lan komutlar� ekranda g�r�nt�le

            while (memory[i] != 43000) { // haf�zada bitirme (halt) komutuna gelinesiye kadar komutlar� s�rayla �al��t�r

                if (!storei_flag && !reti_flag && !addi_flag && !subi_flag && !divi_flag && !muli_flag) { // e�er kod haf�zas�ndan standart opcode-adres okumas� yap�l�yorsa
                    opcode = memory[i] / 1000; // �al��t�r�lacak SML dili komutu
                    address = (long long int)memory[i] % 1000; // bu komutun arg�man�
                }
                else { // e�er kod haf�zas�ndan do�rudan bir adrese veya yazmaca (�rn., ak�m�lat�r) y�klenecek de�er okumas� yap�l�yorsa

                    if (storei_flag) { // haf�zaya hemen bir de�er yaz�lmas� (immediate operation)

                        if (address <= CODEMEMSIZE - 1) // kod haf�zas�ndan bir adrese hemen bir de�er yaz�lacaksa
                            memory[address] = memory[i];
                        else { // e�er yazma�lardan bir tanesine hemen bir de�er yaz�lacaksa

                            if (address == ACCADDRESS) // ak�m�lat�r�n adresi ise
                                accumulator = memory[i];
                            else if (INREGSTARTADDRESS <= address && address <= INREGENDADDRESS) // girdi yazma�lar�ndan birinin adresi ise
                                inreg[address - INREGSTARTADDRESS] = memory[i];
                        }
                        storei_flag = 0; // haf�zaya do�rudan de�er yaz�lmas� bayra��n�n s�f�rlanmas�
                    }

                    if (reti_flag) { // hemen bir de�er d�nd�r�lmesi
                        outreg[0] = memory[i];
                        reti_flag = 0; // hemen de�er d�nd�r�lmesi bayra��n�n s�f�rlanmas�
                    }

                    if (addi_flag) { // hemen bir de�er eklenmesi
                        accumulator += memory[i];
                        addi_flag = 0; // hemen de�er eklenmesi bayra��n�n s�f�rlanmas�
                    }

                    if (subi_flag) { // hemen bir de�er ��kar�lmas�
                        accumulator -= memory[i];
                        subi_flag = 0; // hemen de�er ��kar�lmas� bayra��n�n s�f�rlanmas�
                    }

                    if (divi_flag) { // hemen bir de�ere b�l�nmesi
                        accumulator /= memory[i];
                        divi_flag = 0; // hemen de�ere b�l�nmesi bayra��n�n s�f�rlanmas�
                    }

                    if (muli_flag) { // hemen bir de�erle �arp�lmas�
                        accumulator *= memory[i];
                        muli_flag = 0; // hemen de�erle �arp�lmas� bayra��n�n s�f�rlanmas�
                    }

                    skip_opcode = 1;
                }

                counter++;

                if (_DEBUG) {
                    printf("\nAccumulator : %.3lf", accumulator); // accumulator yazmac�n�n o andaki de�erini yazd�r�r
                    printf("\nOpcode      : %lld", opcode); // komutu yazd�r�r
                    printf("\nOperand     : %lld", address); // komutun arg�man� olan adresi yazd�r�r
                    printf("\nCounter     : %d\n", counter); // sayac� yazd�r�r
                }

                if (!skip_opcode) { // e�er hemen i�lem (immediate operation) yap�lm�yorsa opcode �al��t�r�l�r

                    switch (opcode) {

                    case 10: // klavyeden say� giri�i ve o say�n�n ilgili haf�za konumuna yerle�tirilmesi
                        printf("Opcode degeri 10'dur, bu yuzden bir sayi girin: ");
                        scanf("%lld", &memory[address]);
                        break;
                    case 11: // ilgili haf�za konumundaki say�n�n ekrana yazd�r�lmas�
                        printf("\n%.3lf\n", memory[address]);
                        break;


                    case 20: // accumulator yazmac�na ilgili haf�za konumundaki veya ba�ka bir yazma�taki de�eri y�kleme
                        if (address <= CODEMEMSIZE - 1) // kod haf�zas�ndan bir adresteki de�er yaz�lacaksa
                            accumulator = memory[address];
                        else { // e�er yazma�lardan bir tanesinin de�eri yaz�lacaksa

                            if (INREGSTARTADDRESS <= address && address <= INREGENDADDRESS) // girdi yazma�lar�ndan birinin adresi ise
                                accumulator = inreg[address - INREGSTARTADDRESS];
                        }
                        break;
                    case 21: // accumulator yazmac�ndaki de�eri ilgili haf�za konumuna veya ba�ka bir yazmaca y�kleme
                        if (address <= CODEMEMSIZE - 1) // kod haf�zas�ndan bir adrese de�er yaz�lacaksa
                            memory[address] = accumulator;
                        else { // e�er yazma�lardan bir tanesine de�er yaz�lacaksa

                            if (INREGSTARTADDRESS <= address && address <= INREGENDADDRESS) // girdi yazma�lar�ndan birinin adresi ise
                                inreg[address - INREGSTARTADDRESS] = accumulator;
                        }
                        break;

                    case 22: // ilgili haf�za konumuna veya bir yazmaca hemen bir de�er y�kleme (immediate operation)
                             // de�er, SML dosyas�nda bu komutun yaz�ld��� sat�r�n hemen ard�ndna gelen sat�rda yaz�lmaktad�r
                             // dolay�s�yla, bilgisayar�n kod haf�zas�nda 22 numaral� komutu i�eren kelimenin (word) sakland���
                             // adresten (SML dosyas�ndaki ilgili sat�rdan) hemen sonra gelen adres konumunda (SML dosyas�nda hemen sonr agelen sat�rda) saklan�r
                             // bunun i�in, kod haf�zas�ndan �ekilecek bir sonraki say�sal de�erin bir komuttan ziyade yukar�da
                             // bahsedildi�i gibi belirtilen bir de�er oldu�unu bilgisayar�n anlamas� ad�na 
                             // bir bayra�� kald�r�r�z (set the logical flag - bir yaz�l�m deyimidir)
                        storei_flag = 1;
                        break;


                    case 30: // ilgili haf�za konumundaki veya yazma�taki de�eri accumulator yazmac�ndaki de�ere ekleme ve sonucu yazma�ta saklama
                        if (address <= CODEMEMSIZE - 1) // kod haf�zas�ndan bir adresteki de�er eklenecekse
                            accumulator += memory[address];
                        else { // e�er yazma�lardan bir tanesinin de�eri eklenecekse

                            if (address == ACCADDRESS) // ak�m�lat�r�n adresi ise
                                accumulator += accumulator;
                            else if (INREGSTARTADDRESS <= address && address <= INREGENDADDRESS) // girdi yazma�lar�ndan birinin adresi ise
                                accumulator += inreg[address - INREGSTARTADDRESS];
                        }
                        break;
                    case 31: // ilgili haf�za konumundaki veya yazma�taki de�eri accumulator yazmac�ndaki de�erden ��karma ve sonucu yazma�ta saklama
                        if (address <= CODEMEMSIZE - 1) // kod haf�zas�ndan bir adresteki de�er ��kar�lacaksa
                            accumulator -= memory[address];
                        else { // e�er yazma�lardan bir tanesinin de�eri ��kar�lacaksa

                            if (address == ACCADDRESS) // ak�m�lat�r�n adresi ise
                                accumulator -= accumulator;
                            else if (INREGSTARTADDRESS <= address && address <= INREGENDADDRESS) // girdi yazma�lar�ndan birinin adresi ise
                                accumulator -= inreg[address - INREGSTARTADDRESS];
                        }
                        break;
                    case 32: // accumulator yazmac�ndaki de�eri ilgili haf�za konumundaki de�ere b�lme ve sonucu yazma�ta saklama
                        if (address <= CODEMEMSIZE - 1) // kod haf�zas�ndan bir adresteki de�ere b�l�necekse
                            accumulator /= memory[address];
                        else { // e�er yazma�lardan bir tanesinin de�erine b�l�necekse

                            if (address == ACCADDRESS) // ak�m�lat�r�n adresi ise
                                accumulator /= accumulator;
                            else if (INREGSTARTADDRESS <= address && address <= INREGENDADDRESS) // girdi yazma�lar�ndan birinin adresi ise
                                accumulator /= inreg[address - INREGSTARTADDRESS];
                        }
                        break;
                    case 33: // ilgili haf�za konumundaki veya yazma�taki de�eri accumulator yazmac�ndaki de�erle �arpma ve sonucu yazma�ta saklama
                        if (address <= CODEMEMSIZE - 1) // kod haf�zas�ndan bir adresteki de�erle �arp�lacaksa
                            accumulator *= memory[address];
                        else { // e�er yazma�lardan bir tanesinin de�eriyle �arp�lacaksa

                            if (address == ACCADDRESS) // ak�m�lat�r�n adresi ise
                                accumulator *= accumulator;
                            else if (INREGSTARTADDRESS <= address && address <= INREGENDADDRESS) // girdi yazma�lar�ndan birinin adresi ise
                                accumulator *= inreg[address - INREGSTARTADDRESS];
                        }
                        break;
                    case 34: // accumulator yazmac�ndaki de�eri 1 artt�r
                        accumulator += 1;
                        break;
                    case 35: // accumulator yazmac�ndaki de�eri 1 azalt
                        accumulator -= 1;
                        break;

                    case 36: // accumulator yazmac�ndaki de�ere hemen bir de�er ekleme (immediate operation) ve sonucu accumulatorde saklama
                        addi_flag = 1;
                        break;
                    case 37: // accumulator yazmac�ndaki de�erden hemen bir de�er ��karma ve sonucu accumulatorde saklama
                        subi_flag = 1;
                        break;
                    case 38: // accumulator yazmac�ndaki de�eri hemen bir de�ere b�lme ve sonucu accumulatorde saklama
                        divi_flag = 1;
                        break;
                    case 39: // accumulator yazmac�ndaki de�eri hemen bir de�erle �arpma ve sonucu accumulatorde saklama
                        muli_flag = 1;
                        break;


                    case 40: // SML komutu 40 (dallanma) oldu�unda ilgili adres arg�man�yla belirtilen haf�za konumuna s��rama
                        i = address;
                        continue;
                        break;
                    case 41: // accumulator yazmac�ndaki de�er negatif ise ilgili adrese ko�ullu s��rama
                        if (accumulator < 0) {
                            i = address;
                            continue;
                        }
                        break;
                    case 42: // accumulator yazmac�ndaki de�er s�f�r ise ilgili adrese ko�ullu s��rama
                        if (accumulator == 0) {
                            i = address;
                            continue;
                        }
                        break;


                    case 50: // ilgili adresteki veya yazma�taki de�eri d�nd�rme - ��kt� yazmac� (outreg) i�ine yaz�ld���nda i�lemci bu de�eri d�nd�rm�� olur
                        if (address <= CODEMEMSIZE - 1) // kod haf�zas�ndan bir adresteki de�er d�nd�r�l�cekse
                            outreg[0] = memory[address];
                        else { // e�er yazma�lardan bir tanesinin de�eri d�nd�r�lecekse
                            // printf("\nadres: %ld, inreg[0]: %ld, inreg[1]: %ld\n", address, (long long int)inreg[0], (long long int)inreg[1]);
                            if (address == ACCADDRESS) // ak�m�lat�r�n adresi ise
                                outreg[0] = accumulator;
                            else if (INREGSTARTADDRESS <= address && address <= INREGENDADDRESS) // girdi yazma�lar�ndan birinin adresi ise
                                outreg[0] = inreg[address - INREGSTARTADDRESS];
                        }
                        break;
                    case 51: // hemen bir de�er d�nd�rme
                        reti_flag = 1;
                        break;
                    } // switch (opcode) sonu

                } // if (!skip_opcode) sonu
                else
                    skip_opcode = 0; // bir sonraki opcode i�lenebilsin diye bayra�� indir

                i++; // program sayac�n� bir artt�rma - bir sonraki �al��t�r�lacak komutun haf�zadaki konumunu belirtir

            } // while (memory[i] != 4300) sonu

            printf("\n\n****Bilgisayar islemini tamamladiktan sonra hafizanin durumu****\n\n");
            displayMemory(memory, 100);
            return 1;

        } // else sonu

    } // if (getcwd(cwd, sizeof(cwd)) != NULL) sonu
    else {
        printf("\nSML dosyasi adresine ulasilamiyor\n");
        return -1;
    }

} // int SML_sim(void) sonu
