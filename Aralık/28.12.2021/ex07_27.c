#include<stdio.h>
#include<stdlib.h>
#include <io.h>

#define PATH_MAX 150

// fonksiyon tanımı: hafızaya yazılmış olan komutları gösterir
// @in: memory - hafızadaki komutları tutan dizi, size - bu dizinin boyutu
// @out: void
void displayMemory(int memory[], int size) {

    int i;

    printf("\n\n****SML dosyasi hafizaya yuklendi****\n\n");

    for (i = 0; i < size; i++) {
        printf(" %+05d ", memory[i]);

        if ((i + 1) % 10 == 0) {
            printf("\n");
        }
    }

    printf("\n");

}

// fonksiyon tanımı: çalıştırılacak ana fonksiyon
void main(void) {

    int memory[100] = { 0 }; // hafızaya yazılacak makine dili komutlarını tutar
    int i = 0; // çalıştırılacak komutun bulunduğu adres değerini tutan program sayacı
    int opcode = 0; // çalıştırılacak komut
    int address = 0; // komutun işleyeceği argüman yani adres
    int accumulator = 0; // aritmetik işlemler için özel yazmaç (register)
    int counter = 0; // SML komutlarını ilgili dosyadan çekip hafızaya kaydederken kullanılan sayaç
    FILE* file; // SML.txt isimli ve SML komutlarını tutan dosyaya işaret eden gösterici (pointer)
    char cwd[PATH_MAX]; // üzerinde çalışılan klasör adresini tutar
    char absolutefilename[PATH_MAX]; // tam yol ile beliritlen dosya ismi

    if (getcwd(cwd, sizeof(cwd)) != NULL) { // SML dosya adresine ulaş, 
                                            // SML.txt dosyası, üzerinde çalışılan proje dosyası (.vcxproj uzantılı) ile aynı klasörde bulundurulmalı

        sprintf(absolutefilename, "%s\\SML-a.txt", cwd);
        printf("\n%s\n", absolutefilename);
        if ((file = fopen(absolutefilename, "r")) == NULL) { // SML dosyasını aç
            printf("\nTemin edilen SML dosyasi acilmiyor\n");
        }
        else {
            // SML dosyasının sonuna gelesiye kadar hafızadan komut-argüman çiftlerini oku ve hafızaya yaz
            while (!feof(file)) {
                fscanf(file, "%d", &memory[i]); // dosyadan hafızaya yazma işlemi

                i++;
            }

            fclose(file); // SML dosyasını kapat
            i = 0;

            displayMemory(memory, 100); // hafızaya yazılan komutları ekranda görüntüle

            while (memory[i] != 4300) { // hafızada bitirme (halt) komutuna gelinesiye kadar komutları sırayla çalıştır

                opcode = memory[i] / 100; // çalıştırılacak SML dili komutu

                address = memory[i] % 100; // bu komutun argümanı

                counter++;

                printf("\nAccumulator : %d", accumulator); // accumulator yazmacının o andaki değerini yazdırır

                printf("\nOpcode      : %d", opcode); // komutu yazdırır

                printf("\nOperand     : %d", address); // komutun argümanı olan adresi yazdırı

                printf("\nCounter     : %d\n", counter); // sayacı yazdırır

                switch (opcode) {

                case 10: // klavyeden sayı girişi ve o sayının ilgili hafıza konumuna yerleştirilmesi
                    printf("Opcode degeri 10'dur, bu yuzden bir sayi girin: ");
                    scanf("%d", &memory[address]);
                    break;
                case 11: // ilgili hafıza konumundaki sayının ekrana yazdırılması
                    printf("\n%d\n", memory[address]);
                    break;
                case 20: // accumulator yazmacına ilgili hafıza konumundaki değeri yükleme
                    accumulator = memory[address];
                    break;
                case 21: // accumulator yazmacındaki değeri ilgili hafıza konumuna yükleme
                    memory[address] = accumulator;
                    break;
                case 30: // ilgili hafıza konumundaki değeri accumulator yazmacındaki değere ekleme ve sonucu yazmaçta saklama
                    accumulator += memory[address];
                    break;
                case 31: // ilgili hafıza konumundaki değeri accumulator yazmacındaki değerden çıkarma ve sonucu yazmaçta saklama
                    accumulator -= memory[address];
                    break;
                case 32: // accumulator yazmacındaki değeri ilgili hafıza konumundaki değere bölme ve sonucu yazmaçta saklama
                    accumulator /= memory[address];
                    break;
                case 33: // ilgili hafıza konumundaki değeri accumulator yazmacındaki değerle çarpma ve sonucu yazmaçta saklama
                    accumulator *= memory[address];
                    break;
                case 40: // SML komutu 40 (dallanma) olduğunda ilgili adres argümanıyla belirtilen hafıza konumuna sıçrama
                    i = address;
                    continue;
                    break;
                case 41: // accumulator yazmacındaki değer negatif ise ilgili adrese koşullu sıçrama
                    if (accumulator < 0) {
                        i = address;
                        continue;
                    }
                    break;
                case 42: // accumulator yazmacındaki değer sıfır ise ilgili adrese koşullu sıçrama
                    if (accumulator == 0) {
                        i = address;
                        continue;
                    }
                    break;
                } // switch (opcode) sonu

                i++; // program sayacını bir arttırma - bir sonraki çalıştırılacak komutun hafızadaki konumunu belirtir

            } // while (memory[i] != 4300) sonu

        } // else sonu

    } // if (getcwd(cwd, sizeof(cwd)) != NULL) sonu
    else {
        printf("\nSML dosyasi adresine ulasilamiyor\n");
    }

 } // void main(void) sonu