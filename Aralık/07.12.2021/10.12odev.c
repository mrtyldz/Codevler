#include <stdio.h>

unsigned packCharacters( char, char );
void displayBits( unsigned );

int main()
{    
    char a, b;
    unsigned result;
    
    printf("Karakter 1'i gir: ");
    scanf("%c", &a);
    fflush(stdin);
    
    printf("Karakter 1'yi gir: ");
    scanf("%c", &b);
    fflush(stdin);
    
    printf("%c önce = ", a);
    displayBits( a );
    printf("%c önce = ", b);
    displayBits( b );
    result = packCharacters(a, b); 
    printf("Sonuç   = ");
    displayBits( result );
    return 0;
}


unsigned packCharacters( char a, char b )
{
    unsigned result = a;
    result <<=8;
    return result|=b;  /* Inclusive OR */
}

void displayBits( unsigned value )
{
    unsigned c, displayMask = 1<<31;
    
    for( c=1; c<=32; c++ )
    {
        putchar( value & displayMask ? '1' : '0' );
        value <<= 1;
        
        if(c % 8 == 0)
            putchar( ' ' );
    }
    
    putchar( '\n' );
}