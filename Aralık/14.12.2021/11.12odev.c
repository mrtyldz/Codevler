#include <stdio.h>
#include<locale.h>

struct tool { 
    int recordNum;
    char toolName[ 30 ];
    int quantity;
    double cost;
};


void initialiseRecords( FILE * );
int enterChoice( void );
void addRecord( FILE * );
void updateRecord( FILE * );
void deleteRecord( FILE *fPtr );
void textFile( FILE *, char * );
void displayRecords( FILE * );
int strcmp2( const char *, const char * );


int main()
{   
    setlocale(LC_ALL, "turkish");
   FILE *cfPtr;
   int choice;
 
    if ( ( cfPtr = fopen( "hardware.dat", "rb+" ) ) == NULL )
    {
        cfPtr = fopen( "hardware.dat", "wb+" );
        fclose( cfPtr );
        cfPtr = fopen( "hardware.dat", "rb+" );
        initialiseRecords( cfPtr );
    }

    while( (choice = enterChoice() ) != 6)
    {
        switch(choice) {
            case 1:
                textFile( cfPtr, "Hardware.txt" );
                break;
            case 2:
                addRecord( cfPtr );
                break;
            case 3:
                updateRecord( cfPtr );
                break;
            case 4:
                deleteRecord( cfPtr );
                break;
            case 5:
                displayRecords( cfPtr );
                break;
            default:
                printf("\nHATALI SECENEK- BASKA SECENEK DENEYIN!\n\n");
                break;    
        }
    }
    
    fclose( cfPtr );

   return 0;
}


void initialiseRecords(FILE *nfPtr)
{
    struct tool blankTool = { 0, "unassigned", 0, 0.0 };
    int i;
    
    rewind( nfPtr );
    
    for(i=0; i<100; i++)
    {   
        blankTool.recordNum = i+1;   
        
        fwrite( &blankTool, sizeof( struct tool ), 1, 
                 nfPtr );     
    }
}


int enterChoice( void )
{
    int menuChoice;
    
    printf("Seciminizi girin:\n"
            "1 - Tüm araç kayıtlarından oluşan bir metin dosyası oluşturun\n"
            " adlı \"tools.txt\"\n"
            "2 - yeni bir kayıt ekle\n"
            "3 - mevcut bir kaydı güncelleyin\n"
            "4 - bir kaydı sil\n"
            "5 - tüm takım kayıtlarını göster\n"
            "6 - programı sonlandır\n?" ); 

    scanf("%d", &menuChoice);
    fflush( stdin );
    
    return menuChoice;
}


void addRecord( FILE *fPtr )
{
    struct tool blankTool = { 0, "", 0, 0.0 };
    int accountNum;
    
    printf("Kayıt numarasını girin (1-100) : ");
    scanf( "%d", &accountNum );
    
    fseek( fPtr, 
           ( accountNum - 1 ) *  sizeof( struct tool ), 
           SEEK_SET );
    fread( &blankTool, sizeof( struct tool ), 1,
           fPtr );
    
    if( strcmp2(blankTool.toolName, "unassigned") != 0)
        printf( "\nBu numaraya sahip bir kayıt zaten var!\n\n");
    else {
        printf( "Takım adını, miktarını, maliyetini girin\n? " );
        scanf( "%s%d%lf", &blankTool.toolName, &blankTool.quantity,
                         &blankTool.cost );   
        fseek( fPtr, 
           ( accountNum - 1 ) *  sizeof( struct tool ), 
           SEEK_SET );
        fwrite( &blankTool, sizeof( struct tool ), 1, 
                 fPtr );   
        printf( "\nKayıt eklendi\n\n");
    }
}


void updateRecord( FILE *fPtr )
{
    struct tool blankTool = { 0, "", 0, 0.0 };
    int accountNum;
    
    printf("Güncellenecek kayıt numarası ( 1-100 ): ");
    scanf( "%d", &accountNum );
    
    fseek( fPtr, 
           ( accountNum - 1 ) *  sizeof( struct tool ), 
           SEEK_SET );
    fread( &blankTool, sizeof( struct tool ), 1,
           fPtr );
    
    if( strcmp2(blankTool.toolName, "unassigned") == 0)
        printf( "\nGüncellenecek kayıtta bilgi yok!\n\n");
    else {
        printf( "Takım adını, miktarını, maliyetini girin\n? " );
        scanf( "%s%d%lf", &blankTool.toolName, &blankTool.quantity,
                         &blankTool.cost );   
        fseek( fPtr, 
           ( accountNum - 1 ) *  sizeof( struct tool ), 
           SEEK_SET );
        fwrite( &blankTool, sizeof( struct tool ), 1, 
                 fPtr );   
        printf( "\nKayıt güncellendi!\n");
    }
}


void deleteRecord( FILE *fPtr )
{
    struct tool temp = { 0, "unassigned", 0, 0.0 };
    struct tool blankTool = { 0, "unassigned", 0, 0.0 };
    
    int recordNum;
    
    printf("Silinecek kaydın numarasını girin ( 1-100 ): ");
    scanf( "%d", &recordNum );
    
    fseek( fPtr, 
           ( recordNum - 1 ) *  sizeof( struct tool ), 
           SEEK_SET );
    fread( &temp, sizeof( struct tool ), 1,
           fPtr );
    
    if( strcmp2(temp.toolName, "unassigned") == 0)
        printf( "\nKayıt herhangi bir bilgi içermiyor!\n\n");
    else {
        fseek( fPtr, 
           ( recordNum - 1 ) *  sizeof( struct tool ), 
           SEEK_SET );
        blankTool.recordNum = recordNum;
        fwrite( &blankTool, sizeof( struct tool ), 1, 
                 fPtr );   
        printf( "\nKayıt silindi!\n");
    }
}


void textFile( FILE *readPtr, char *filename )
{
    struct tool blankTool = { 0, "", 0, 0.0 };
    FILE *writePtr;
 
    if ( ( writePtr = fopen( filename, "w" ) ) == NULL )
        printf( "Dosya açılamadı.\n" );
    else { 
        rewind( readPtr );
        fprintf( writePtr, "%-12s%-32s%-11s%10s\n",
              "Rekor Numarası", "Araç Adı", "Miktar", "Maliyet");
              
        fread( &blankTool, sizeof( struct tool ), 1, readPtr );    
              
        while ( !feof( readPtr  ) ) { 
            fprintf( writePtr, "%-12d%-32s%-11d%10.2f\n", 
                    blankTool.recordNum, blankTool.toolName,
                    blankTool.quantity, blankTool.cost   );    
                    
            fread( &blankTool, sizeof( struct tool ), 1, 
                readPtr );           
        }
              
        fclose( writePtr );
        printf("\nDosya \"%s\" oluşturuldu\n\n",filename );
    }  
}


void displayRecords( FILE *readPtr )
{
    //struct person blankPerson = { "unassigned", "", "0"};
    struct tool blankTool = { 0, "", 0, 0.0 };
    int numberOfRecords = 0;
 
    rewind( readPtr );

    printf( "\n%-12s%-32s%-11s%10s\n",
          "Rekor Numarası", "Araç Adı", "Miktar", "Maliyet");
          
    fread( &blankTool, sizeof( struct tool ), 1, readPtr );  
          
    while ( !feof( readPtr  ) ) {
        if( strcmp2(blankTool.toolName, "unassigned") != 0)
        {
            printf( "%-12d%-32s%-11d%10.2f\n", 
                    blankTool.recordNum, blankTool.toolName,
                    blankTool.quantity, blankTool.cost   );  
            
            numberOfRecords++;  
        }
        fread( &blankTool, sizeof( struct tool ), 1, 
                        readPtr );  
    }
    
    if(numberOfRecords==0)
        printf("\nGösterilecek kayıt yok!\n");
    printf("\n");
}


int strcmp2( const char *s1, const char *s2 )
{      
    while(*s2!='\0') {
        if(*s1<*s2)
            return -1;
        else if(*s1>*s2)
            return 1;
        s1++;
        s2++;           
    }

    return 0;   
}